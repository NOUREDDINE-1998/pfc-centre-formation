<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formateur extends Model
{
    use HasFactory;

    protected $fillable = [
        
        'cin',
        'nom',
        'prenom',
        'dateNaissance', 
        'adresse',
        'email',
        'sexe',
        'tel',
        'niveauDetude',
        'experience',
        'cv',
        'photo',
        'sessions'
       
    ];

    // protected $with = ['sessions'];
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
}
