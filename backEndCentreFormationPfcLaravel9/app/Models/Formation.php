<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;
    
    protected $fillable = ['prix', 'description', 'totalHeures'];

    protected $with = ['sessions'];
    
    
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function formateurs()
    {
        return $this->hasManyThrough(Formateur::class,Session::class);
    }
}
