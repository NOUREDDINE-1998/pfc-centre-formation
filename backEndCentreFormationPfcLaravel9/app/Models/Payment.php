<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'typePayment',
        'montant',
        'rubrique',
        'inscription'
    ];

    public function inscription()
    {
        return $this->hasOne(Inscription::class);
    }
}
