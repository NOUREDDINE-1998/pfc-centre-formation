<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'formateur_id',
        'formation_id',
        'description',
        'dateDebut',
        'dateFin',
        'horaire',
        'formation',
        'formateur'
       
    ];
    
    use HasFactory;

    

    public function formation()
    {
        return $this->belongsTo(Formation::class);
    }

    public function formateur()
    {
        return $this->belongsTo(Formateur::class);
    }

    public function beneficiaries()
    {
        return $this->belongsToMany(Beneficiary::class,'inscriptions');
    }
    
}
