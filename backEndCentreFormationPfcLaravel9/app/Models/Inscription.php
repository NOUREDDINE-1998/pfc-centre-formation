<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    use HasFactory;
   
    

    
    protected $fillable = ['session_id', 'beneficiary_id', 'dateInscription'];

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }
    
}
