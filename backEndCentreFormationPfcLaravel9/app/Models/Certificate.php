<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use HasFactory;
    protected $fillable = ['inscription_id ', 'centre', 'dateObtention','imprime'];
    
    public function inscription()
    {
        return $this->belongsTo(Inscription::class);
    }

}
