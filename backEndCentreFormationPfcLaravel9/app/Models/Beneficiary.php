<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    use HasFactory;
    protected $fillable = [
        
        'cin',
        'nom',
        'prenom',
        'dateNaissance',
        'adresse',
        'email',
        'sexe',
        'tel',
        'niveauDetude',     
        'photo',
        'typeBeneficiaire',
        'sessions'
       
    ];
    
    public function sessions()
    {
        return $this->belongsToMany(Session::class,'inscriptions')->withPivot('dateInscription', 'created_at');
    }
}
