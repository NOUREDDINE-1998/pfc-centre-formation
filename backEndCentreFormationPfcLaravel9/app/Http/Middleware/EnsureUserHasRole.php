<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;
use Illuminate\Http\Request;

class EnsureUserHasRole
{
    public function handle(Request $request, Closure $next, )
    {
        if(Auth::check()){
            if(auth()->user()->tokenCan('server:admin')){
                return $next($request);
            }
            else{
                return response()->json([
                    'status'=>401,
                    'message'=>'Access Denied ,you are not an Admin',
                 ],401);
            }
        }
        else{
            return response()->json([
                'status'=>201,
                'message'=>'return to login',
             ]);
        }
        // if($request->user()->roles()->where('name','admin')->exists()){
        //     return $next($request);
        // }
        // abort(403);
    }
}
