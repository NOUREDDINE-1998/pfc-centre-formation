<?php

namespace App\Http\Controllers;

use App\Models\Inscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Session;
use App\Models\Beneficiary;


class InscriptionController extends Controller
{
    
    public function index()
    {
        try {
            $inscriptions=Inscription::all();
         
           return response()->json([
               'status'=>200,
               'inscriptions'=>$inscriptions,
             
               
           ]);
       } catch (Exception $e) {
           Log::error($e);
       }
    }

    
   
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'session_id'=>'required|numeric',
            'beneficiary_id'=>'required|numeric',
            'dateInscription'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                  

                $inscription= new Inscription;
                $session=Session::find($request->get('session_id'));
                $beneficiaire=Beneficiary::find($request->get('beneficiary_id'));
                // $inscription->session_id=$request->get('session_id');
                // $inscription->beneficiary_id=$request->input('beneficiary_id');
                $inscription->dateInscription=$request->input('dateInscription');
                $inscription->session_id=$session->id;
                $inscription->beneficiary_id=$beneficiaire->id;
                $inscription->save();

                return response()->json([
                    'status'=>200,
                    'message'=>'Inscription added successfully',
                    'sess'=>Session::find($request->get('session_id'))
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
                
            }
        }
                  
    }

   
    

   
    public function edit( $id)
    {
        $inscription= Inscription::find($id);
        if($inscription){
           return response()->json([
               'status'=>200,
               'inscription'=>$inscription
            ]);
        }
        else{
           return response()->json([
               'status'=>404,
               'message'=>'no Inscription found'
            ]);
        }
    }

    
    
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'session_id'=>'required|numeric',
            'beneficiary_id'=>'required|numeric',
            'dateInscription'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
           
            $inscription= Inscription::find($id);
            $inscription->session_id=$request->get('session_id');
            $inscription->beneficiary_id=$request->input('beneficiary_id');
            $inscription->dateInscription=$request->input('dateInscription');
    
            $inscription->update();
            return response()->json([
                'status'=>200,
                'message'=>'inscription updated successfully'
             ]);
        }
        
    }

   
    public function destroy( $id)
    {
        Inscription::where('id',$id)->delete();
        return response()->json([
            'status'=>200,
            'message'=>'Inscription deleted successfully'
         ]);
    }

    public function search($key){

        $inscriptions= Inscription::where('id','LIKE','%'.$key.'%')->get();
 
         return response()->json([
             'status'=>200,
             'inscriptions'=>$inscriptions
          ]);
 
     }
}
