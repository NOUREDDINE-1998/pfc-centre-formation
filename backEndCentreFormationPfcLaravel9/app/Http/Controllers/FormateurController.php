<?php

namespace App\Http\Controllers;

use App\Models\Formateur;
use App\Models\Formation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class FormateurController extends Controller
{
    
    public function index()
    {
        try {
            //$formateurs=Formateur::all();
            $formateurs=Formateur::with('sessions')->get();
            return response()->json([
                'status'=>200,
                'formateurs'=>$formateurs
            ]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

   
    public function store(Request $request)
    {
       
        $validator=Validator::make($request->all(),[
            'cin' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
            'dateNaissance' => 'required',
            'adresse' => 'required',
            'email' => 'required',
            'sexe' => 'required',
            'tel' => 'required',
            'niveauDetude' => 'required',
            'experience' => 'required',
            'cv' => 'required|mimes:pdf,docx|max:2048',
             'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            $input = $request->all();

            $imageName='';
            $cvName='';
            if($request->hasFile(('photo'))){
                $imageName = time(). '.'.$request->file('photo')->getClientOriginalExtension();
                $request->file('photo')->move('images/', $imageName);
              
            }else{
                return $imageName=NULL;
            }
    
            if($request->hasFile(('cv'))){
                $cvName = time(). '.'.$request->file('cv')->getClientOriginalExtension();
                $request->file('cv')->move('cvs/', $cvName);
              
            }else{
                return $cvName=NULL;
            }
        
            $input['photo'] = $imageName;
            $input['cv'] = $cvName;      
            Formateur::create($input);
            
            return response()->json([
                'status'=>200,
                'message'=>'added successfully'
            ]);
        }
    }

   
   
    public function edit( $id)
    {
      //  $formateur= Formateur::find($id);
        // $sess=$formateur->sessions;
        $formateur=Formateur::with('sessions')->where('id',$id)->get();
        //  $f=$formateur->sessions()->where('id',3)->first();
        //  $descr=$f->formation_id;
        //  $test=Formation::find($descr);
         if($formateur->count()!=0){
            return response()->json([
                'status'=>200,
                'formateur'=>$formateur,    
               
             ]);
         }
         else{
            return response()->json([
                'status'=>404,
                'message'=>'no formateur found'
             ]);
         }
        
    }

    
    public function update(Request $request,  $id)
    {
        $validator=Validator::make($request->all(),[
            'cin' => 'required',
             'nom' => 'required',
             'prenom' => 'required',
             'dateNaissance' => 'required',
             'adresse' => 'required',
             'email' => 'required',
             'sexe' => 'required',
             'tel' => 'required',
             'niveauDetude' => 'required',
             'experience' => 'required',
            
         ]);
 
         if($validator->fails()){
             return response()->json([
                 'validateError'=>$validator->messages()
             ]);
         }
         else{
            $formateur= Formateur::find($id);
             $input = $request->all();
            $imageName="";
            $cvName='';

                // photo
               $destination=public_path('images/'.$formateur->photo);  
            if($request->hasFile(('photo'))){
                File::delete($destination);  
                $imageName = time(). '.'.$request->file('photo')->getClientOriginalExtension();
                $request->file('photo')->move('images/', $imageName);       
            }else{
                 $imageName=$formateur->photo;
            }

                // cv
                $destination1=public_path('cvs/'.$formateur->cv);  
                if($request->hasFile(('cv'))){
                    File::delete($destination1);  
                    $cvName = time(). '.'.$request->file('cv')->getClientOriginalExtension();
                    $request->file('cv')->move('cvs/', $cvName);       
                }else{
                     $cvName=$formateur->cv;
                }
            
                    $input['photo'] = $imageName;  
                    $input['cv'] = $cvName;      
                    $formateur->update($input);
                    return response()->json([
                        'status'=>200,
                        'message'=>'updated successfully'
                    ]);
         }
        
    }


    public   function destroy( $id)
    {
        $formateur= Formateur::find($id);
          // photo
               
          $destination=public_path('images/'.$formateur->photo);        
              File::delete($destination);           
          
             // cv
            $destination1=public_path('cvs/'.$formateur->cv);
            File::delete($destination1);

             $deleted=$formateur->delete();
             if($deleted){
                return response()->json([
                    'status'=>200,
                 
                    'message'=>'deleted successfully'
                ]);
             }

         
    }

    public function search($key){
        $formateur= Formateur::where('cin','LIKE','%'.$key.'%')->get();
 
         return response()->json([
             'status'=>200,
             'formateurs'=>$formateur
          ]);
 
     }
}
