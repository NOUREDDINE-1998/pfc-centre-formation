<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Inscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CertificateController extends Controller
{
  
    public function index()
    {
        try {
            $certificates=Certificate::all();
         
           return response()->json([
               'status'=>200,
               'certificates'=>$certificates,
             
               
           ]);
       } catch (Exception $e) {
           Log::error($e);
       }
    }

   
    public function create()
    {
        $validator=Validator::make($request->all(),[
            'inscription_id'=>'required',
            'centre'=>'required',
            'dateObtention'=>'required',
            'imprime'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $certificate= new Certificate;
                $inscription= Inscription::find($request->get('inscription_id'));

                $certificate->inscription_id=$inscription->id;
                $certificate->centre=$request->get('centre');
                $certificate->dateObtention=$request->input('dateObtention');
                $certificate->imprime=$request->input('imprime');
                $certificate->save();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'certificate added successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
        }
    }

 
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'inscription_id'=>'required',
            'centre'=>'required',
            'dateObtention'=>'required',
            // 'imprime'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $certificate= new Certificate;
                $inscription= Inscription::find($request->get('inscription_id'));

                $certificate->inscription_id=$inscription->id;
                $certificate->centre=$request->get('centre');
                $certificate->dateObtention=$request->input('dateObtention');
                $certificate->imprime=0 ;
                $certificate->save();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'certificate added successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
        }
    }


  
    public function edit( $id)
    {
        $certificate= Certificate::find($id);
        if($certificate){
           return response()->json([
               'status'=>200,
               'certificate'=>$certificate
            ]);
        }
        else{
           return response()->json([
               'status'=>404,
               'message'=>'no certificate found'
            ]);
        }
    }

  
    public function update(Request $request,  $id)
    {
        $validator=Validator::make($request->all(),[
            'inscription_id'=>'required',
            'centre'=>'required',
            'dateObtention'=>'required',
            'imprime'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $certificate=  Certificate::find($id);
                $inscription= Inscription::find($request->get('inscription_id'));

                $certificate->inscription_id=$inscription->id;
                $certificate->centre=$request->get('centre');
                $certificate->dateObtention=$request->input('dateObtention');
                $certificate->imprime=$request->input('imprime');
                $certificate->update();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'certificate updated successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
        }
    }

  
    public function destroy( $id)
    {
          Certificate::where('id',$id)->delete();
        return response()->json([
            'status'=>200,
            'message'=>'Certificate deleted successfully'
         ]);
    }


    
    public function search($key){
        $certificates= Certificate::where('inscription_id','LIKE','%'.$key.'%')->get();
 
         return response()->json([
             'status'=>200,
             'certificates'=>$certificates
          ]);
 
     }
}
