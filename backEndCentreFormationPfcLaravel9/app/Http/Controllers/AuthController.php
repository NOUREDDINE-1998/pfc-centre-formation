<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
class AuthController extends Controller
{
    public function register(Request $request){

        $validator=Validator::make($request->all(),[
            'name' => 'required',
             'email' => 'required|email',
             'password' => 'required|min:8',
             
 
         ]);
 
         if($validator->fails()){
             return response()->json([   
                 'validateError'=>$validator->messages()
             ]);
         }
         else{
            $user=User::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'role_as'=>$request->role_as,

            ]);
            $token= $user->createToken($user->email.'_Token',[] )->plainTextToken;

            return response()->json([   
               'status'=>200,
               'username'=>$user->name,
               'token'=>$token,
               'message'=>'Registered Successfully'
            ]);

         }
    }

    public function login(Request $request){
        $validator=Validator::make($request->all(),[
             'email' => 'required|email',
             'password' => 'required|min:8',
             
 
         ]);
 
         if($validator->fails()){
             return response()->json([   
                 'validateError'=>$validator->messages()
             ]);
         }
         else{
 
            $user = User::where('email', $request->email)->first();
            $role='';
           
            if (! $user || ! Hash::check($request->password, $user->password)) {
                return response()->json([   
                'status'=>401,
                'message'=>'Invaliid Credentials'
                ]);
            }
            else{
                // if($user->name=='admin'){
                //     $role='admin';
                // };
                $role=$user->role_as;
                if($role==1){
                    $token= $user->createToken($user->email.'_AdminToken',['server:admin'] )->plainTextToken;

                }else{
                    $token= $user->createToken($user->email.'_Token',[] )->plainTextToken;

                }
                return response()->json([   
                    'status'=>200,
                    'username'=>$user->name,
                    'token'=>$token,
                    'role'=>$role,
                    'message'=>'Logged In Successfully'
                 ]);
            }  
         }
    }

    public function logout(Request $request){   
        request()->user()->currentAccessToken()->delete();
        return response()->json([   
            'status'=>200,
            'message'=>'Logged out Successfully',           
         ]);
    }
}
