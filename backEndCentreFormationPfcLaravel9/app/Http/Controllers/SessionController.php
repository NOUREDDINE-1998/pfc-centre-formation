<?php

namespace App\Http\Controllers;

use App\Models\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SessionController extends Controller
{
    
    public function index()
    {
        try {
            $sessions=Session::all();
            return response()->json([
                'status'=>200,
                'sessions'=>$sessions
            ]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'formateur_id'=>'required',
            'formation_id'=>'required',
            'description'=>'required',
            'dateDebut'=>'required',
            'dateFin'=>'required',
            'horaire'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $session= new Session;
                $session->formateur_id=$request->get('formateur_id');
                $session->formation_id=$request->get('formation_id');
                $session->description=$request->get('description');
                $session->dateDebut=$request->get('dateDebut');
                $session->dateFin=$request->get('dateFin');
                $session->horaire=$request->get('horaire');

                $session->save();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'session added successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
        }
        
            
    }

   
  
    public function edit( $id)
    {
        $session= Session::find($id);
        
        if($session){
           return response()->json([
               'status'=>200,
               'session'=>$session,
              
            ]);
        }
        else{
           return response()->json([
               'status'=>404,
               'message'=>'no session found'
            ]);
        }
    }

    public function update(Request $request,  $id)
    {
        $validator=Validator::make($request->all(),[
            'formateur_id'=>'required',
            'formation_id'=>'required',
            'description'=>'required',
            'dateDebut'=>'required',
            'dateFin'=>'required',
            'horaire'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $session=  Session::find($id);
                $session->formateur_id=$request->get('formateur_id');
                $session->formation_id=$request->get('formation_id');
                $session->description=$request->get('description');
                $session->dateDebut=$request->get('dateDebut');
                $session->dateFin=$request->get('dateFin');
                $session->horaire=$request->get('horaire');

                $session->update();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'session updated successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
        }
    }

  
    public function destroy( $id)
    {
        Session::where('id',$id)->delete();
        return response()->json([
            'status'=>200,
            'message'=>'Session deleted successfully'
         ]);
    }

    public function search($key){
        $sessions= Session::where('description','LIKE','%'.$key.'%')->get();
 
         return response()->json([
             'status'=>200,
             'sessions'=>$sessions
          ]);
 
     }
}
