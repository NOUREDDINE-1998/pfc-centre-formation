<?php

namespace App\Http\Controllers;

use App\Models\beneficiary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class BeneficiaryController extends Controller
{
    
    public function index()
    {
        try {
            $beneficiaires=Beneficiary::all();
            return response()->json([
                'status'=>200,
                'beneficiaires'=>$beneficiaires
            ]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

   
    
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'cin' => 'required',
             'nom' => 'required',
             'prenom' => 'required',
             'dateNaissance' => 'required',
             'adresse' => 'required',
             'email' => 'required',
             'sexe' => 'required',
             'tel' => 'required',
             'niveauDetude' => 'required',
             'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
             'typeBeneficiaire'=>'required',
 
         ]);
 
         if($validator->fails()){
             return response()->json([
                
                 'validateError'=>$validator->messages()
             ]);
         }
         else{
             $input = $request->all();
 
             $imageName='';
             if($request->hasFile(('photo'))){
                $imageName = time(). '.'.$request->file('photo')->getClientOriginalExtension();
                $request->file('photo')->move('images/', $imageName);
              
            }else{
                return $imageName=NULL;
            }

     
 
             $input['photo'] = $imageName;
             Beneficiary::create($input);
             return response()->json([
                 'status'=>200,
                 'message'=>'added successfully'
             ]);
         }
    }

    

  
    public function edit( $id)
    {
        $beneficiary= Beneficiary::find($id);
   
         if($beneficiary){
            return response()->json([
                'status'=>200,
                'beneficiary'=>$beneficiary,
              
             ]);
         }
         else{
            return response()->json([
                'status'=>404,
                'message'=>'no beneficiary found'
             ]);
         }
        
    }

   
    public function update(Request $request,  $id)
    {
        $validator=Validator::make($request->all(),[
            'cin' => 'required',
             'nom' => 'required',
             'prenom' => 'required',
             'dateNaissance' => 'required',
             'adresse' => 'required',
             'email' => 'required',
             'sexe' => 'required',
             'tel' => 'required',
             'niveauDetude' => 'required',
         ]);
 
         if($validator->fails()){
             return response()->json([
                 'validateError'=>$validator->messages()
             ]);
         }
         else{
            $beneficiary= Beneficiary::find($id);
             $input = $request->all();
            $imageName="";
                // photo
            
            $destination=public_path('images/'.$beneficiary->photo);  
            if($request->hasFile(('photo'))){
                File::delete($destination);  
                $imageName = time(). '.'.$request->file('photo')->getClientOriginalExtension();
                $request->file('photo')->move('images/', $imageName);       
            }else{
                 $imageName=$beneficiary->photo;
            }

          
             $input['photo'] = $imageName;      
             $beneficiary->update($input);
             return response()->json([
                 'status'=>200,
                 'message'=>'updated successfully'
             ]);
         }
        
    }

  
    public function destroy( $id)
    {
        $beneficiary= Beneficiary::find($id);


          // photo
          $destination=public_path('images/'.$beneficiary->photo);        
              File::delete($destination);           

             $deleted=$beneficiary->delete();
             if($deleted){
                return response()->json([
                    'status'=>200,
                    'message'=>'deleted successfully'
                ]);
             }

         
    }

    public function search($key){
        $beneficiary= Beneficiary::where('cin','LIKE','%'.$key.'%')->get();
 
         return response()->json([
             'status'=>200,
             'beneficiaires'=>$beneficiary
          ]);
 
     }
}
