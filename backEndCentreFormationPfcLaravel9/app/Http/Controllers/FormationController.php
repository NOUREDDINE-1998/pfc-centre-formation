<?php

namespace App\Http\Controllers;

use App\Models\Formation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
             $formations=Formation::all();
          
            //  $formations = $formations->except([ 3]);
            // $formations = Formation::withOnly('sessions')->get();
            // $formations = Formation::without('sessions')->get();
           // $formationss=Formation::with('sessions')->get();
        //    $formations = DB::table('formations')->get();
        // $c=$formations->count();
            return response()->json([
                'status'=>200,
                'formations'=>$formations,
              
                
            ]);
        } catch (Exception $e) {
            Log::error($e);
        }
        
    }

  
   
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'description'=>'required|max:50',
            'prix'=>'required|numeric',
            'totalHeures'=>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $formation= new Formation;
                $formation->description=$request->get('description');
                $formation->prix=$request->input('prix');
                $formation->totalHeures=$request->input('totalHeures');
                $formation->save();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'formation added successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
        }
                  
    }

    public function edit($id)
    {
         $formation= Formation::find($id);
         if($formation){
            return response()->json([
                'status'=>200,
                'formation'=>$formation
             ]);
         }
         else{
            return response()->json([
                'status'=>404,
                'message'=>'no formation found'
             ]);
         }
        
    }

   
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'description'=>'required|max:50',
            'prix'=>'required|numeric',
            'totalHeures'=>'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            $formation= Formation::find($id);
            $formation->description=$request->input('description');
            $formation->prix=$request->input('prix');
            $formation->totalHeures=$request->input('totalHeures');
    
            $formation->update();
            return response()->json([
                'status'=>200,
                'message'=>'formation updated successfully'
             ]);
        }
        
    }

    
    public function destroy( $id)

    {
        
        Formation::where('id',$id)->delete();
        return response()->json([
            'status'=>200,
            'message'=>'formation deleted successfully'
         ]);
    }


    public function search($key){
       $formation= Formation::where('description','LIKE','%'.$key.'%')->get();

        return response()->json([
            'status'=>200,
            'formations'=>$formation
         ]);

    }
}
