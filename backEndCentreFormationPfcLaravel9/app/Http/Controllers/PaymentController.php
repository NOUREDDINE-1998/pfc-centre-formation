<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Inscription;
use App\Models\Session;
use App\Models\Beneficiary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    
    public function index()
    {
        try {
            $payments=Payment::all();
            return response()->json([
                'status'=>200,
                'payments'=>$payments
            ]);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

   
    

    
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'typePayment'=>'required',
            'montant'=>'required',
            'rubrique'=>'required',
            'inscription_id'=>'required',
            
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $payment= new Payment;
                $inscription=Inscription::find($request->get('inscription_id'));

                $payment->typePayment=$request->get('typePayment');
                $payment->montant=$request->get('montant');
                $payment->rubrique=$request->get('rubrique');
                $payment->inscription_id=$inscription->id;
                

                $payment->save();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'payment added successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
    }
    }
   
    
    
    public function edit( $id)
    {
        $payment= Payment::find($id);
        if($payment){
           return response()->json([
               'status'=>200,
               'payment'=>$payment
            ]);
        }
        else{
           return response()->json([
               'status'=>404,
               'message'=>'no payment found'
            ]);
        }
    }

   
    public function update(Request $request,  $id)
    {
       
        $validator=Validator::make($request->all(),[
            'typePayment'=>'required',
            'montant'=>'required',
            'rubrique'=>'required',
            'inscription_id'=>'required',
            
        ]);

        if($validator->fails()){
            return response()->json([
               
                'validateError'=>$validator->messages()
            ]);
        }
        else{
            try {
                $payment=  Payment::find($id);
                $inscription=Inscription::find($request->get('inscription_id'));

                $payment->typePayment=$request->get('typePayment');
                $payment->montant=$request->get('montant');
                $payment->rubrique=$request->get('rubrique');
                $payment->inscription_id=$inscription->id;
                

                $payment->update();
               
                return response()->json([
                    'status'=>200,
                    'message'=>'payment updated successfully'
                ]);
            
            } catch (Exception $e) {
                Log::error($e);
            }
    }
    }

    
    public function destroy( $id)
    {
        Payment::where('id',$id)->delete();
        return response()->json([
            'status'=>200,
            'message'=>'Payment deleted successfully'
         ]);
    }

    public  function recu($id)
    {
        $payment= Payment::find($id);
        $inscription=Inscription::find($payment->inscription_id);
        $session=Session::find($inscription->session_id);
        $beneficiary=Beneficiary::find($inscription->beneficiary_id);

        $nomPrenom=$beneficiary->prenom.' '.$beneficiary->nom;
        $cin=$beneficiary->cin;
        $sessionDescription=$session->description;
        if($payment){
           return response()->json([
               'status'=>200,
               'nomPrenom'=>$nomPrenom,
               'cin'=>$cin,
               'sessionDescription'=>$sessionDescription,
              'payment'=>$payment,

            //    'session'=>$session,
            //    'beneficiary'=>$beneficiary
            ]);
        }
        else{
            return  response()->json('no trouvé', 200,'');
        }

    }
}
