<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formateurs', function (Blueprint $table) {
            $table->id();
            $table->string('cin', 10)->unique();
            $table->string('nom');
            $table->string('prenom');
            $table->date('dateNaissance');
            $table->string('adresse', 100);
            $table->string('email')->unique();
            $table->string('sexe');
            $table->string('tel')->unique();;
            $table->string('niveauDetude');
            $table->integer('experience')->unsigned();
            $table->string('cv')->unique();
            $table->string('photo')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formateurs');
    }
};
