<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum','role'])->group(function () {
Route::get('/certificates', [App\Http\Controllers\CertificateController::class, 'index']);

});

// user
Route::post('/login', [App\Http\Controllers\AuthController::class, 'login']);
Route::post('/register', [App\Http\Controllers\AuthController::class, 'register']);

Route::middleware('auth:sanctum')->group( function () {
    // Route::get('/chekingAuthenticated', function(){
    //     return response()->json([
    //         'status'=>200,
    //         'message'=>'You are in'
    //     ],200);
    // });


    Route::post('/logout', [App\Http\Controllers\AuthController::class, 'logout']);

// Certificate
// Route::get('/certificates', [App\Http\Controllers\CertificateController::class, 'index']);
Route::post('/add-certificate', [App\Http\Controllers\CertificateController::class, 'store']);
Route::get('/edit-certificate/{id}', [App\Http\Controllers\CertificateController::class, 'edit']);
Route::post('/update-certificate/{id}', [App\Http\Controllers\CertificateController::class, 'update']);
Route::delete('/delete-certificate/{id}', [App\Http\Controllers\CertificateController::class, 'destroy']);
Route::get('/certificates/{key}', [App\Http\Controllers\CertificateController::class, 'search']);

// formation
Route::get('/formations', [App\Http\Controllers\FormationController::class, 'index']);
Route::get('/formations/{key}', [App\Http\Controllers\FormationController::class, 'search']);
Route::post('/add-formation', [App\Http\Controllers\FormationController::class, 'store']);
Route::get('/edit-formation/{id}', [App\Http\Controllers\FormationController::class, 'edit']);
Route::put('/update-formation/{id}', [App\Http\Controllers\FormationController::class, 'update']);
Route::delete('/delete-formation/{id}', [App\Http\Controllers\FormationController::class, 'destroy']);

// formateur
Route::get('/formateurs', [App\Http\Controllers\FormateurController::class, 'index']);
Route::post('/add-formateur', [App\Http\Controllers\FormateurController::class, 'store']);
Route::patch('/edit-formateur/{id}', [App\Http\Controllers\FormateurController::class, 'edit']);
Route::post('/update-formateur/{id}', [App\Http\Controllers\FormateurController::class, 'update']);
Route::delete('/delete-formateur/{id}', [App\Http\Controllers\FormateurController::class, 'destroy']);
Route::get('/formateurs/{key}', [App\Http\Controllers\FormateurController::class, 'search']);

// beneficiaires
Route::get('/beneficiaires', [App\Http\Controllers\BeneficiaryController::class, 'index']);
Route::post('/add-Beneficiary', [App\Http\Controllers\BeneficiaryController::class, 'store']);
Route::get('/edit-Beneficiary/{id}', [App\Http\Controllers\BeneficiaryController::class, 'edit']);
Route::post('/update-Beneficiary/{id}', [App\Http\Controllers\BeneficiaryController::class, 'update']);
Route::delete('/delete-Beneficiary/{id}', [App\Http\Controllers\BeneficiaryController::class, 'destroy']);
Route::get('/beneficiaires/{key}', [App\Http\Controllers\BeneficiaryController::class, 'search']);

// session
Route::get('/sessions', [App\Http\Controllers\SessionController::class, 'index']);
Route::post('/add-session', [App\Http\Controllers\SessionController::class, 'store']);
Route::get('/edit-session/{id}', [App\Http\Controllers\SessionController::class, 'edit']);
Route::post('/update-session/{id}', [App\Http\Controllers\SessionController::class, 'update']);
Route::delete('/delete-session/{id}', [App\Http\Controllers\SessionController::class, 'destroy']);
Route::get('/sessions/{key}', [App\Http\Controllers\SessionController::class, 'search']);

// Payment
Route::get('/payments', [App\Http\Controllers\PaymentController::class, 'index']);
Route::post('/add-payment', [App\Http\Controllers\PaymentController::class, 'store']);
Route::get('/edit-payment/{id}', [App\Http\Controllers\PaymentController::class, 'edit']);
Route::post('/update-payment/{id}', [App\Http\Controllers\PaymentController::class, 'update']);
Route::delete('/delete-payment/{id}', [App\Http\Controllers\PaymentController::class, 'destroy']);
Route::get('/recu/{id}', [App\Http\Controllers\PaymentController::class, 'recu']);

// Inscription
Route::get('/inscriptions', [App\Http\Controllers\InscriptionController::class, 'index']);
Route::post('/add-inscription', [App\Http\Controllers\InscriptionController::class, 'store']);
Route::get('/edit-inscription/{id}', [App\Http\Controllers\InscriptionController::class, 'edit']);
Route::post('/update-inscription/{id}', [App\Http\Controllers\InscriptionController::class, 'update']);
Route::delete('/delete-inscription/{id}', [App\Http\Controllers\InscriptionController::class, 'destroy']);
Route::get('/inscriptions/{key}', [App\Http\Controllers\InscriptionController::class, 'search']);


});

// middleware
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


