import React, { useState , useEffect}  from 'react';
import  { Link ,useParams  } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function UpdateFormateur (props){
  
    const params = useParams();
    const [formateur, setFormateur] = useState({
        cin:"",
        nom:"",
        prenom:"",
        dateNaissance:"", 
        adresse:"",
        email:"",
        tel:"",
        niveauDetude:"",
        experience:"",
    });

     const [photo, setPhoto] = useState('');
     const [cv, setCv] = useState('');
     const [error, setError] = useState([]);
     const [sexe, setSexe] = useState('');
   
         
     const handleInput=(e)=>{
        e.persist();
          setFormateur({...formateur,[e.target.name]:e.target.value})
    }
    
    const handlePhoto=(e)=>{
        setPhoto(e.target.files[0])
      
    }
    const handleCv=(e)=>{
        setCv({cv:e.target.value})
      
    }
    const handleMale=(e)=>{
        setSexe(e.target.value)
      
    }
    const handleFemale=(e)=>{
        setSexe(e.target.value)
      
    }

     useEffect(() => {       
           
           axios.patch('http://127.0.0.1:8000/api/edit-formateur/'+params.id).then((res)=>{

            if(res.data.status===200){
                setFormateur(res.data.formateur[0]);
                 setCv(res.data.formateur[0].cv)
                 setPhoto(res.data.formateur[0].photo)
                setSexe(res.data.formateur[0].sexe);

           }
           else if(res.data.status===404){
            swal({
                title: "Warning!",
                text: res.data.message,
                icon: "Warning",
                button: "Ok!",
              });

           }
           
           })
         
          },[params]);
     
          const updateFormateur=(e)=>{
                 e.preventDefault();
                 const formData = new FormData() ;
                        formData.append('cv', cv);
                        formData.append('photo', photo);
                        formData.append('cin', formateur.cin);
                        formData.append('nom', formateur.nom);
                        formData.append('prenom', formateur.prenom);
                        formData.append('dateNaissance', formateur.dateNaissance);
                        formData.append('adresse', formateur.adresse);
                        formData.append('email', formateur.email);
                        formData.append('sexe', sexe);
                        formData.append('tel', formateur.tel);
                        formData.append('niveauDetude', formateur.niveauDetude);
                        formData.append('experience', formateur.experience);


                 axios.post('http://127.0.0.1:8000/api/update-formateur/'+params.id,formData).then((res)=>{
                    if(res.data.status===200){
                        swal({
                            title: "Updated!",
                            text: res.data.message,
                            icon: "success",
                            button: "Ok!",
                          });
                       
                        console.log(res.data.message)
                    }
                    else{
                        setError(res.data.validateError);
                       }
                 })
                
            }

    return (
        
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                    <div className="card">
                        <div className="card-header">
                            <h4>
                                Formateur Data
                            <Link to={'/formateur'} className='btn btn-primary float-end btn-sm'> back  </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <form onSubmit={updateFormateur} encType="multipart/form-data">
                        <div className="mb-3">
                            <label htmlFor="cin" className="form-label">Cin</label>
                            <input type="text" className="form-control" id="cin" name='cin' value={formateur.cin} onChange={handleInput} />
                            <span className='text-danger'>{error.cin}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="nom" className="form-label">Nom</label>
                            <input type="text" className="form-control" id="nom" name='nom' value={formateur.nom} onChange={handleInput}/>
                            <span className='text-danger'>{error.nom}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="prenom" className="form-label">Prenom</label>
                            <input type="text" className="form-control" id="prenom" name='prenom' value={formateur.prenom} onChange={handleInput} />
                            <span className='text-danger'>{error.prenom}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateNaissance" className="form-label">date Naissance</label>
                            <input type="date" className="form-control" id="dateNaissance" name='dateNaissance' value={formateur.dateNaissance} onChange={handleInput}/>
                            <span className='text-danger'>{error.dateNaissance}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="adresse" className="form-label">adresse</label>
                            <input type="text" className="form-control" id="adresse" name='adresse' value={formateur.adresse} onChange={handleInput} />
                            <span className='text-danger'>{error.adresse}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">email</label>
                            <input type="email" className="form-control" id="email" name='email' value={formateur.email} onChange={handleInput}/>
                            <span className='text-danger'>{error.email}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="sexe" className="form-label">sexe</label>
                            <div className="form-check">
                                <input className="form-check-input" type="radio"  id="male" value='male'
                                 checked={sexe==='male'}  onChange={handleMale} />
                                <label className="form-check-label" htmlFor="male">
                                    male
                                </label>
                                </div>
                                <div className="form-check">
                                <input className="form-check-input" type="radio" id="female" value ="female" 
                                checked={sexe==='female'}  onChange={handleFemale} />
                                <label className="form-check-label" htmlFor="female">
                                    female
                                </label>
                            </div>
                            {/* <input type="text" className="form-control" id="sexe" name='sexe' value={formateurs.sexe} onChange={handleInput}/> */}
                            <span className='text-danger'>{error.sexe}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="tel" className="form-label">tel</label>
                            <input type="text" className="form-control" id="tel" name='tel' value={formateur.tel} onChange={handleInput} />
                            <span className='text-danger'>{error.tel}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="niveauDetude" className="form-label">niveau Detude</label>
                            <input type="text" className="form-control" id="niveauDetude" name='niveauDetude' value={formateur.niveauDetude} onChange={handleInput} />
                            <span className='text-danger'>{error.niveauDetude}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="experience" className="form-label">experience</label>
                            <input type="number" className="form-control" id="experience" name='experience' value={formateur.experience} onChange={handleInput} />
                            <span className='text-danger'>{error.experience}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="cv" className="form-label">cv</label>
                            <input type="file"  className="form-control" id="cv" name='cv' onChange={handleCv}/>
                            <span className='text-danger'>{error.cv}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="photo" className="form-label">photo</label>
                            <input type="file"  className="form-control" id="photo" name='photo'  onChange={handlePhoto} />
                            <span className='text-danger'>{error.photo}</span>
                        </div>

                        <button type="submit" className="btn btn-primary">update</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                    <div className='col-md-6'>
                    <img src={ "http://127.0.0.1:8000/images/"+photo ?? '' } height='400px' width='400px'  className="card-img-top rounded-circle" alt="test"/>

                    </div>
                </div>
            </div>
        );
    
}

export default UpdateFormateur;