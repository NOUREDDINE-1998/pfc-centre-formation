import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Formateur extends Component {
    constructor(props){
        super(props);
        this.state={
            formateurs:[],
            loading:true,
            currentKeyword:''
        }
    }
componentDidMount(){
    this.getformateurList(this.state.currentKeyword);
}

getformateurList=async (keyword)=>{
    const res= await axios.get('http://127.0.0.1:8000/api/formateurs/'+keyword);

        if(res.data.status===200){
            this.setState({
                formateurs: res.data.formateurs,
               loading:false
              });
       }   
}

search=(keyword)=>{
    
    this.getformateurList(keyword);
   this.setState({
        currentKeyword:keyword
   });
}
    render() {
        var formateursTableHTML='';
        if(this.state.loading===true){
            formateursTableHTML=<tr><td colSpan="7"><h2 >Loading...</h2></td></tr>
        }
        else if(this.state.formateurs.length===0){
            formateursTableHTML=<tr><td colSpan="7" ><h2 className='text text-danger'>non formateur trouvé</h2></td></tr>
        }
        else{
            formateursTableHTML= this.state.formateurs.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }


        return (
            <div className='container-fluid'>
                <div className='row '>
                <SearchBar dsearch={this.search}/>

                    <div className='col-md-12 '>
                    <div className="card">
                        <div className="card-header">
                            <h4>
                            Formateur Data
                            <Link to={'/add-formateur'} className='btn btn-primary float-end btn-sm'> Add Formateur</Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover'>
                            <thead>
                                <tr>
                                <th>#</th>
                                <th> Photo</th>
                                <th>Cin</th>
                                <th>Nom & Prenom</th>
                                <th>Date Naissance</th>
                                <th>Adresse</th>
                                <th>Email</th>
                                <th>Sexe </th>
                                <th> Tel</th>
                                <th>Niveau D'etude</th>
                                <th>Experience</th>
                                <th>curriculum vitae </th>
                                <th>Actions </th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                    {formateursTableHTML}    
                            </tbody>
                     </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Formateur;