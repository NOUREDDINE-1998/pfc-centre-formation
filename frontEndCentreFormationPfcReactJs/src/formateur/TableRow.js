import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class TableRow extends Component {  

    constructor(props){
        super(props);
       
    }

    delete=(e,id)=>{
        e.preventDefault();
        let deleted=e.currentTarget;
        deleted.innerText="deleted";
        axios.delete('http://127.0.0.1:8000/api/delete-formateur/'+id)
           .then((res)=>{
            if(res.data.status===200){
                swal({
                    title: "Deleted!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });
                deleted.closest('tr').remove();
         
           }
           })
    }
  
    render() {
        return (
            
            <tr>
            <td>{this.props.data.id}</td>
            <td><img src={ "http://127.0.0.1:8000/images/" + this.props.data.photo } height='100px' width='100px' alt='test'/></td>
            <td>{this.props.data.cin}</td>
            <td>{this.props.data.nom +" "+ this.props.data.prenom} </td>
            <td>{this.props.data.dateNaissance}</td>
            <td>{this.props.data.adresse}</td>
            <td>{this.props.data.email}</td>
            <td>{this.props.data.sexe}</td>
            <td>{this.props.data.tel}</td>
            <td>{this.props.data.niveauDetude}</td>
            <td>{this.props.data.experience}</td>
            <td><a href={"http://127.0.0.1:8000/cvs/" +this.props.data.cv} target='_blank'><button className='btn btn-success'>show C.V</button></a></td>
           
            <td>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                            
            <Link to={'/update-formateur/'+this.props.data.id} className='btn btn-warning float-end btn-sm' > Edit</Link>
            <button type="button" className="btn btn-danger" onClick={(e)=>{this.delete(e,this.props.data.id)}}>Delete</button>    

            </div>
            </td>
            
            </tr> 
        );
    }
}

export default TableRow;