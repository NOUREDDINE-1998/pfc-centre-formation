
import React, { useState }  from 'react';
import  { Link   } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function AddFormateur (props)  {
    const [formateurs, setFormateurs] = useState({
        cin:"",
        nom:"",
        prenom:"",
        dateNaissance:"", 
        adresse:"",
        email:"",
        tel:"",
        niveauDetude:"",
        experience:"",
    });
     const [picture, setPicture] = useState([]);
     const [doc, setDoc] = useState([]);
     const [error, setError] = useState([]);
     const [sexe, setSexe] = useState('male');
    
        
     const handleInput=(e)=>{
        e.persist();
          setFormateurs({...formateurs,[e.target.name]:e.target.value})
    }
    
    const handlePhoto=(e)=>{
        setPicture({photo:e.target.files[0]})
      
    }
    const handleCv=(e)=>{
        setDoc({cv:e.target.files[0]})
      
    }
    const handleMale=(e)=>{
        setSexe(e.target.value)
      
    }
    const handleFemale=(e)=>{
        setSexe(e.target.value)
      
    }
   
    const saveFormateur = (e)=>{
        e.preventDefault();
        const formData = new FormData() ;
        formData.append('cv', doc.cv);
        formData.append('photo', picture.photo);
        formData.append('cin', formateurs.cin);
        formData.append('nom', formateurs.nom);
        formData.append('prenom', formateurs.prenom);
        formData.append('dateNaissance', formateurs.dateNaissance);
        formData.append('adresse', formateurs.adresse);
        formData.append('email', formateurs.email);
        formData.append('sexe', sexe);
        formData.append('tel', formateurs.tel);
        formData.append('niveauDetude', formateurs.niveauDetude);
        formData.append('experience', formateurs.experience);

        // axios({
        //     method: "post",
        //     url: "http://127.0.0.1:8000/api/add-formateur",
        //     data: formData,
        //     headers: { "Content-Type": "multipart/form-data" },
        //   }).then(function (response) {
        //       //handle success
        //       console.log(response.data.status);
        //     }).catch(function (response) {
        //       //handle error
        //       console.log(response);
        //     });
       
         axios.post('http://127.0.0.1:8000/api/add-formateur',formData).then(res=>{
            console.log(res.data.status) ;     
            if(res.data.status===200){
                
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });
                    

                  setFormateurs({...formateurs,
                    cin:"",
                    nom:"",
                    prenom:"",
                    dateNaissance:"", 
                    adresse:"",
                    email:"",
                    sexe:"",
                    tel:"",
                    niveauDetude:"",
                    experience:"",
                });

            }
            else{
                setError(res.data.validateError);
            }
           
         })
         
    }
   
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                                Formateur Data
                            <Link to={'/formateur'} className='btn btn-primary float-end btn-sm'> back   </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={saveFormateur}  encType="multipart/form-data">
                        <div className="mb-3">
                            <label htmlFor="cin" className="form-label">Cin</label>
                            <input type="text" className="form-control" id="cin" name='cin' value={formateurs.cin} onChange={handleInput} />
                            <span className='text-danger'>{error.cin}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="nom" className="form-label">Nom</label>
                            <input type="text" className="form-control" id="nom" name='nom' value={formateurs.nom} onChange={handleInput}/>
                            <span className='text-danger'>{error.nom}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="prenom" className="form-label">Prenom</label>
                            <input type="text" className="form-control" id="prenom" name='prenom' value={formateurs.prenom} onChange={handleInput} />
                            <span className='text-danger'>{error.prenom}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateNaissance" className="form-label">date Naissance</label>
                            <input type="date" className="form-control" id="dateNaissance" name='dateNaissance' value={formateurs.dateNaissance} onChange={handleInput}/>
                            <span className='text-danger'>{error.dateNaissance}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="adresse" className="form-label">adresse</label>
                            <input type="text" className="form-control" id="adresse" name='adresse' value={formateurs.adresse} onChange={handleInput} />
                            <span className='text-danger'>{error.adresse}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">email</label>
                            <input type="email" className="form-control" id="email" name='email' value={formateurs.email} onChange={handleInput}/>
                            <span className='text-danger'>{error.email}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="sexe" className="form-label">sexe</label>
                            <div className="form-check">
                                <input className="form-check-input" type="radio"  id="male" value='male'
                                 checked={sexe==='male'}  onChange={handleMale} />
                                <label className="form-check-label" htmlFor="male">
                                    male
                                </label>
                                </div>
                                <div className="form-check">
                                <input className="form-check-input" type="radio" id="female" value ="female" 
                                checked={sexe==='female'}  onChange={handleFemale} />
                                <label className="form-check-label" htmlFor="female">
                                    female
                                </label>
                            </div>
                            {/* <input type="text" className="form-control" id="sexe" name='sexe' value={formateurs.sexe} onChange={handleInput}/> */}
                            <span className='text-danger'>{error.sexe}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="tel" className="form-label">tel</label>
                            <input type="text" className="form-control" id="tel" name='tel' value={formateurs.tel} onChange={handleInput} />
                            <span className='text-danger'>{error.tel}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="niveauDetude" className="form-label">niveau Detude</label>
                            <input type="text" className="form-control" id="niveauDetude" name='niveauDetude' value={formateurs.niveauDetude} onChange={handleInput} />
                            <span className='text-danger'>{error.niveauDetude}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="experience" className="form-label">experience</label>
                            <input type="number" className="form-control" id="experience" name='experience' value={formateurs.experience} onChange={handleInput} />
                            <span className='text-danger'>{error.experience}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="cv" className="form-label">cv</label>
                            <input type="file"  className="form-control" id="cv" name='cv'  onChange={handleCv}/>
                            <span className='text-danger'>{error.cv}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="photo" className="form-label">photo</label>
                            <input type="file"  className="form-control" id="photo" name='photo'  onChange={handlePhoto} />
                            <span className='text-danger'>{error.photo}</span>
                        </div>

                        <button type="submit" className="btn btn-primary">save</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }


export default AddFormateur;