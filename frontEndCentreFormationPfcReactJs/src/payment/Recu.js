import React, { Component } from 'react';
import axios from 'axios';

class Recu extends Component {

    state=({
        payment:[],
        nomPrenom:'',
        cin:'',
        sessionDescription:'',
    })

    //  getPaymentDetails=async()=>{
    //     const res= await axios.get('http://127.0.0.1:8000/api/edit-payment/'+this.props.id);    
    //     if(res.data.status===200){
    //         this.setState({
    //             payment:res.data.payment
    //         })
    //         console.log(this.state.payment);
    //         console.log(this.state.payment.inscription_id)
    //    }
    //  }
     getSessBenDetails=async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/recu/'+this.props.id); 

        if(res.data.status===200){
            this.setState({
                payment:res.data.payment,
                nomPrenom:res.data.nomPrenom,
                cin:res.data.cin,
                sessionDescription:res.data.sessionDescription

            })
       }
     }

    componentDidMount=async()=>{
    //await this.getPaymentDetails();
    await this.getSessBenDetails();

    }

 

    render() {
        var current = new Date();
        var date = `${current.getDate()}/${current.getMonth()+1}/${current.getFullYear()}`;
        return (
            <div className='container mb-3'>
                <div className='row'>
                {/* <div className='col'>
                    <img src=".\logo192.png" alt="test" width="100" height="100"/>
                </div> */}
                <div className='col'>
                    <h4 className='d-flex flex-row-reverse  text-decoration-underline'>Date : {date}</h4>
                </div>
                </div>
                <table className='row justify-content-center'>
                    <thead className='col-3'>
                    
                    <tr><th>Nom & Prenom :</th> </tr>
                    <br/>
                    <tr><th>Cin :</th> </tr>
                    <br/>
                     <tr>   <th>Session :</th></tr>
                     <br/>
                      <tr>  <th>Payment :</th></tr>
                      <br/>
                      <tr>  <th>Rubrique :</th></tr>
                      <br/>
                      <tr>  <th>Montant :</th></tr>
                   
                        
                    </thead>
                    <tbody className='col-3' >
                        
                           <tr> <td>{this.state.nomPrenom}</td></tr>
                           <br/>
                           <tr> <td>{this.state.cin}</td></tr>
                           <br/>
                           <tr> <td>{this.state.sessionDescription}</td></tr>
                           <br/>
                          <tr> <td>{this.state.payment.typePayment}</td></tr>
                          <br/>
                          <tr> <td>{this.state.payment.rubrique}</td></tr>
                          <br/>
                           <tr> <td>{this.state.payment.montant}  dh</td> </tr> 
                      
                    </tbody>
                </table>
                <br/>
                <h4 className='d-flex flex-row-reverse text-decoration-underline'>Cachet et Signature :</h4>
            </div>
        );
    }
}

export default Recu;

