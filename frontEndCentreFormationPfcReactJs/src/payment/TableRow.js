import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';


class TableRow extends Component {  

    constructor(props){
        super(props);

        this.state={
            inscription:[],
            session:[],
            beneficiary:[],
            loading:true
        }
       
    }

 sessionIdDescription=()=>{
    axios.get('http://127.0.0.1:8000/api/edit-session/'+this.state.inscription.session_id).then((res)=>{
        console.log(res.data)
        if(res.data.status===200){          
               this.setState({
               session:res.data.session,
               loading:false
                      }) 
                      console.log(res.data)
                   } 
                   }) 
}

beneficiaryId=()=>{
    axios.get('http://127.0.0.1:8000/api/edit-Beneficiary/'+this.state.inscription.beneficiary_id).then((res)=>{
        console.log(res.data)
        if(res.data.status===200){          
               this.setState({
               beneficiary:res.data.beneficiary,
               loading:false
                      }) 
                      
                   } 
                   }) 
}


componentDidMount=async()=>{
    axios.get('http://127.0.0.1:8000/api/edit-inscription/'+this.props.data.inscription_id).then((res)=>{
        console.log(res.data)
         if(res.data.status===200){
             
             this.setState({
                inscription:res.data.inscription
             })
             this.sessionIdDescription();
             this.beneficiaryId();
        }
        })
       
}

    delete=(e,id)=>{
        e.preventDefault();
        let deleted=e.currentTarget;
        deleted.innerText="deleted";
        axios.delete('http://127.0.0.1:8000/api/delete-payment/'+id)
           .then((res)=>{
            if(res.data.status===200){
                swal({
                    title: "Deleted!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });
                deleted.closest('tr').remove();
         
           }
           })
    }

    render() {
        return (
            
            <tr>
            <td>{this.props.data.id}</td>
            <td>{this.state.loading===true ? 'loading...' :this.state.session.description}</td>
            <td>{this.state.loading===true ? 'loading...' :this.state.beneficiary.prenom+' '+this.state.beneficiary.nom }</td>
            <td>{this.props.data.rubrique}</td>
            <td>{this.props.data.typePayment}</td>
            <td>{this.props.data.montant}</td>
            <td>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                <Link to={'/print/'+this.props.data.id} className='btn btn-success float-end btn-sm' > VIEW to Print</Link>
                <Link to={'/update-payment/'+this.props.data.id} className='btn btn-warning float-end btn-sm' > Edit</Link>
                <button type="button" className="btn btn-danger" onClick={(e)=>{this.delete(e,this.props.data.id)}}>Delete</button>    
               
            </div>
            </td>
            
            </tr> 
        );
    }
}

export default TableRow;