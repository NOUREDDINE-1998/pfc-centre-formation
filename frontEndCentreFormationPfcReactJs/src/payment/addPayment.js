
import React, { useState ,useEffect}  from 'react';
import  { Link   } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function AddPayment (props)  {
    const [payment, setPayment] = useState({
        inscription_id:'',
        typePayment:'',
        rubrique:'',
        montant:''	,
    });
     
     const [error, setError] = useState([]);
     const [inscriptionList, setinscriptionList] = useState([]);
     
  
     const inscription= async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/inscriptions');        
        if(res.data.status===200){
            setinscriptionList(res.data.inscriptions)
       }
     }
     
        useEffect(() => {
            inscription();
        }, []);



     const handleInput=(e)=>{
        e.persist();
          setPayment({...payment,[e.target.name]:e.target.value})
    }
    
   
   
   
    const savepayment = (e)=>{
        e.preventDefault();
        const formData = new FormData() ;
        formData.append('inscription_id', payment.inscription_id);
        formData.append('typePayment', payment.typePayment);
        formData.append('rubrique', payment.rubrique);
        formData.append('montant', payment.montant);
       
         axios.post('http://127.0.0.1:8000/api/add-payment',formData).then(res=>{
            if(res.data.status===200){
                
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });
                    

                  setPayment({...payment,
                    inscription_id:'',
                    typePayment:'',
                    rubrique:'',
                    montant:'',
                });

            }
            else{
                setError(res.data.validateError);
            }
           
         })
         
    }
   
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                            payment Data
                            <Link to={'/payment'} className='btn btn-primary float-end btn-sm'> back </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={savepayment} >

                        <div className="mb-3">
                            <label htmlFor="inscription_id" className="form-label">inscription_id</label>
                            <select className="form-select"   name='inscription_id' onChange={handleInput} value={payment.inscription_id}>
                                <option >choisir inscription_id</option>
                                {
                                    inscriptionList.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id} >{value.id}</option>
                                        )
                                    })
                                }
                                </select>
                            <span className='text-danger'>{error.inscription_id}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="rubrique" className="form-label">rubrique</label>
                            <select className="form-select"   name='rubrique' onChange={handleInput} value={payment.rubrique}>
                                <option >choisir rubrique</option>
                                <option  value="Inscription"  >Inscription</option>
                                <option   value="Scolarite" >Scolarite</option>
                                <option   value="Autre" >Autre</option>
                                </select>
                            <span className='text-danger'>{error.rubrique}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="typePayment" className="form-label">type de Payment</label>
                            <select className="form-select"   name='typePayment' onChange={handleInput} value={payment.typePayment}>
                                <option >choisir type Payment</option>
                                <option  value="Espece"  >Espece</option>
                                <option   value="Versement" >Versement</option>
                                <option   value="Cheque " >Cheque</option>
                                <option   value="Banque " >Banque</option>
                                
                                </select>
                            <span className='text-danger'>{error.typePayment}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="montant" className="form-label">montant</label>
                            <input type="number" className="form-control" id="montant" name='montant' value={payment.montant} onChange={handleInput} />
                            <span className='text-danger'>{error.montant}</span>
                        </div>
                        <button type="submit" className="btn btn-primary">save</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }


export default AddPayment;