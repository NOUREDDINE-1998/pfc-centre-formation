import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Payment extends Component {
    constructor(props){
        super(props);
        this.state={
            payments:[],
            loading:true
        }
    }
componentDidMount(){
    this.getPaymentList();
}

getPaymentList=async ()=>{
    const res= await axios.get('http://127.0.0.1:8000/api/payments');

        if(res.data.status===200){
            this.setState({
                payments: res.data.payments,
               loading:false
              });
       }
        
    
}
    render() {
        var paymentsTableHtml='';
        if(this.state.loading===true){
            paymentsTableHtml=<tr><td colSpan="7"><h2>Loading...</h2></td></tr>
        }
        else{
            paymentsTableHtml= this.state.payments.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }


        return (
            <div className='container'>
                <div className='row offset-1'>
                <SearchBar dsearch={this.search}/>
                    <div className='col-md-10 '>
                    <div className="card">
                        <div className="card-header">
                           
                            <h4>
                                Payment Data
                            <Link to={'/add-payment'} className='btn btn-primary float-end btn-sm'> Add Payment</Link>

                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover'>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Session</th>
                    <th>Beneficiary</th>
                    <th>Rubrique</th>
                    <th>Type payment</th>
                    <th>Montant (dh) </th>
                    <th>Actions</th>
                    
                    </tr>
                </thead>
                <tbody>
                   
                         {paymentsTableHtml}    
                </tbody>
            </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Payment;