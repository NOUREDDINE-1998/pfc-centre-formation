import React, { useRef } from 'react';
import { useReactToPrint } from 'react-to-print';
import  { useParams ,Link } from 'react-router-dom';
import  Recu  from './Recu';

const Example = () => {
    const params=useParams();
  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
});

  return (

    <div className='container '>
      <Recu ref={componentRef}  id={params.id}/>
      <button onClick={handlePrint} className='btn btn-success'>Print!</button>
      <Link to={'/payment'} className='btn btn-danger  ms-4'> back </Link>

    </div>

  );
}
export default Example;

