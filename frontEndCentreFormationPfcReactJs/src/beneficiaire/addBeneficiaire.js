
import React, { useState }  from 'react';
import  { Link   } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function AddBeneficiaire (props)  {
    const [beneficiaire, setBeneficiaire] = useState({
        cin:"",
        nom:"",
        prenom:"",
        dateNaissance:"", 
        adresse:"",
        email:"",
        tel:"",
        niveauDetude:"",
    });
    const [typeBeneficiaire, setTypeBeneficiaire] = useState('');
     const [picture, setPicture] = useState([]);
     const [error, setError] = useState([]);
     const [sexe, setSexe] = useState('male');
    
        
     const handleInput=(e)=>{
        e.persist();
          setBeneficiaire({...beneficiaire,[e.target.name]:e.target.value})
    }
    
    const handlePhoto=(e)=>{
        setPicture({photo:e.target.files[0]})
      
    }
   
    const handleMale=(e)=>{
        setSexe(e.target.value)
      
    }
    const handleFemale=(e)=>{
        setSexe(e.target.value)
      
    }

    const handleoption=(e)=>{
        setTypeBeneficiaire(e.target.value)
      
    }

   
    const saveBeneficiaire= (e)=>{
        e.preventDefault();
        const formData = new FormData() ;
        formData.append('photo', picture.photo);
        formData.append('cin', beneficiaire.cin);
        formData.append('nom', beneficiaire.nom);
        formData.append('prenom', beneficiaire.prenom);
        formData.append('dateNaissance', beneficiaire.dateNaissance);
        formData.append('adresse', beneficiaire.adresse);
        formData.append('email', beneficiaire.email);
        formData.append('sexe', sexe);
        formData.append('tel', beneficiaire.tel);
        formData.append('niveauDetude', beneficiaire.niveauDetude);
        formData.append('typeBeneficiaire', typeBeneficiaire);

      
         axios.post('http://127.0.0.1:8000/api/add-Beneficiary',formData).then(res=>{
            console.log(res.data) ;     
            if(res.data.status===200){
                
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });
                    

                  setBeneficiaire({...beneficiaire,
                    cin:"",
                    nom:"",
                    prenom:"",
                    dateNaissance:"", 
                    adresse:"",
                    email:"",
                    sexe:"",
                    tel:"",
                    niveauDetude:"",
                    typeBeneficiaire:"",
                });

            }
            else{
                setError(res.data.validateError);
            }
           
         })
         
    }
   
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                            Beneficiaire Data
                            <Link to={'/beneficiaire'} className='btn btn-primary float-end btn-sm'> back   </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={saveBeneficiaire}  encType="multipart/form-data">
                        <div className="mb-3">
                            <label htmlFor="cin" className="form-label">Cin</label>
                            <input type="text" className="form-control" id="cin" name='cin' value={beneficiaire.cin} onChange={handleInput} />
                            <span className='text-danger'>{error.cin}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="nom" className="form-label">Nom</label>
                            <input type="text" className="form-control" id="nom" name='nom' value={beneficiaire.nom} onChange={handleInput}/>
                            <span className='text-danger'>{error.nom}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="prenom" className="form-label">Prenom</label>
                            <input type="text" className="form-control" id="prenom" name='prenom' value={beneficiaire.prenom} onChange={handleInput} />
                            <span className='text-danger'>{error.prenom}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateNaissance" className="form-label">date Naissance</label>
                            <input type="date" className="form-control" id="dateNaissance" name='dateNaissance' value={beneficiaire.dateNaissance} onChange={handleInput}/>
                            <span className='text-danger'>{error.dateNaissance}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="adresse" className="form-label">adresse</label>
                            <input type="text" className="form-control" id="adresse" name='adresse' value={beneficiaire.adresse} onChange={handleInput} />
                            <span className='text-danger'>{error.adresse}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">email</label>
                            <input type="email" className="form-control" id="email" name='email' value={beneficiaire.email} onChange={handleInput}/>
                            <span className='text-danger'>{error.email}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="sexe" className="form-label">sexe</label>
                            <div className="form-check">
                                <input className="form-check-input" type="radio"  id="male" value='male'
                                 checked={sexe==='male'}  onChange={handleMale} />
                                <label className="form-check-label" htmlFor="male">
                                    male
                                </label>
                                </div>
                                <div className="form-check">
                                <input className="form-check-input" type="radio" id="female" value ="female" 
                                checked={sexe==='female'}  onChange={handleFemale} />
                                <label className="form-check-label" htmlFor="female">
                                    female
                                </label>
                            </div>
                            <span className='text-danger'>{error.sexe}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="tel" className="form-label">tel</label>
                            <input type="text" className="form-control" id="tel" name='tel' value={beneficiaire.tel} onChange={handleInput} />
                            <span className='text-danger'>{error.tel}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="niveauDetude" className="form-label">niveau Detude</label>
                            <input type="text" className="form-control" id="niveauDetude" name='niveauDetude' value={beneficiaire.niveauDetude} onChange={handleInput} />
                            <span className='text-danger'>{error.niveauDetude}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="typeBeneficiaire" className="form-label">typeBeneficiaire</label>
                            <select className="form-select"   name='typeBeneficiaire' onChange={handleoption} value={typeBeneficiaire}>
                                <option >choisir type beneficiaire</option>
                                <option selected={typeBeneficiaire==='normal'} value="normal"  >normal</option>
                                <option selected={typeBeneficiaire==='entreprise'}  value="entreprise" >entreprise</option>
                                </select>
                            <span className='text-danger'>{error.typeBeneficiaire}</span>
                        </div>                  
                        <div className="mb-3">
                            <label htmlFor="photo" className="form-label">photo</label>
                            <input type="file"  className="form-control" id="photo" name='photo'  onChange={handlePhoto} />
                            <span className='text-danger'>{error.photo}</span>
                        </div>

                        <button type="submit" className="btn btn-primary">save</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }


export default AddBeneficiaire;