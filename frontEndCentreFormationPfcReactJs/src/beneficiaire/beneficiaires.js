import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Beneficiaire extends Component {
    constructor(props){
        super(props);
        this.state={
            beneficiaires:[],
            loading:true,
            currentKeyword:''
        }
    }
componentDidMount(){
    this.getbeneficiairesList(this.state.currentKeyword);
}

getbeneficiairesList=async (keyword)=>{
    const res= await axios.get('http://127.0.0.1:8000/api/beneficiaires/'+keyword);

        if(res.data.status===200){
            this.setState({
                beneficiaires: res.data.beneficiaires,
               loading:false
              });
       } 
}

search=(keyword)=>{
    
    this.getbeneficiairesList(keyword);
   this.setState({
        currentKeyword:keyword
   });
}
    render() {
        var beneficiairesTableHTML='';
        if(this.state.loading===true){
            beneficiairesTableHTML=<tr><td colSpan="7"><h2 >Loading...</h2></td></tr>
        }
        else if(this.state.beneficiaires.length===0){
            beneficiairesTableHTML=<tr><td colSpan="7" ><h2 className='text text-danger'>non beneficiair trouvé</h2></td></tr>
        }
        else{
            beneficiairesTableHTML= this.state.beneficiaires.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }


        return (
            <div className='container-fluid'>
                <div className='row '>
                <SearchBar dsearch={this.search}/>

                    <div className='col-md-12 '>
                    <div className="card">
                        <div className="card-header">
                            <h4>
                            Beneficiaire Data
                            <Link to={'/add-beneficiaire'} className='btn btn-primary float-end btn-sm'> Add Beneficiaire</Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover'>
                            <thead>
                                <tr>
                                <th>#</th>
                                <th> Photo</th>
                                <th>Cin</th>
                                <th>Nom & Prenom</th>
                                <th>Date Naissance</th>
                                <th>Adresse</th>
                                <th>Email</th>
                                <th>Sexe </th>
                                <th> Tel</th>
                                <th>Niveau D'etude</th>
                                <th>typeBeneficiaire</th>
                                <th>Actions </th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                    {beneficiairesTableHTML}    
                            </tbody>
                     </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Beneficiaire;