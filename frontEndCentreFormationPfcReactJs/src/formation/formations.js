import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Formation extends Component {
    constructor(props){
        super(props);
        this.state={
            formations:[],
            loading:true,
            currentKeyword:''
        }
    }
componentDidMount(){
    this.getFormationList(this.state.currentKeyword);
}

getFormationList=async (keyword)=>{
    const res= await axios.get('http://127.0.0.1:8000/api/formations/'+keyword);

        if(res.data.status===200){
            this.setState({
                formations: res.data.formations,
               loading:false
              });
       }}

search=(keyword)=>{
    
     this.getFormationList(keyword);
    this.setState({
         currentKeyword:keyword
    });
}
    render() {
        var formationsTableHtml='';
        if(this.state.loading===true){
            formationsTableHtml=<tr><td colSpan="7"><h2>Loading...</h2></td></tr>
        }
        else{
            formationsTableHtml= this.state.formations.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }
        return (
            <div className='container '>
               
                <div className='row offset-1'>
                <SearchBar dsearch={this.search}/>
                    <div className='col-md-10 '>
                    <div className="card">
                        <div className="card-header">
                           
                            <h4>
                            Formation Data
                            <Link to={'/add-formation'} className='btn btn-primary float-end btn-sm'> Add Formation</Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover' >
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Description</th>
                    <th>Prix (DH)</th>
                    <th>Total Heures</th>
                    <th>Actions</th>
                    
                    </tr>
                </thead>
                <tbody>
                   
                         {formationsTableHtml}    
                </tbody>
            </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Formation;