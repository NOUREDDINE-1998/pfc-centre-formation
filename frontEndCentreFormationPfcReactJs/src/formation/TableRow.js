import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class TableRow extends Component {  

    constructor(props){
        super(props);
       
    }
    delete=(e,id)=>{
        e.preventDefault();
        let deleted=e.currentTarget;
        deleted.innerText="deleted";
        axios.delete('http://127.0.0.1:8000/api/delete-formation/'+id)
           .then((res)=>{
            if(res.data.status===200){
                swal({
                    title: "Deleted!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });
                deleted.closest('tr').remove();
         
           }
           })
    }

    render() {
        return (
            
            <tr>
            <td>{this.props.data.id}</td>
            <td>{this.props.data.description}</td>
            <td>{this.props.data.prix}</td>
            <td>{this.props.data.totalHeures}</td>
            <td>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                            
                <Link to={'/update-formation/'+this.props.data.id} className='btn btn-warning float-end btn-sm' > Edit</Link>
                <button type="button" className="btn btn-danger" onClick={(e)=>{this.delete(e,this.props.data.id)}}>Delete</button>    
               
            </div>
            </td>
            
            </tr> 
        );
    }
}

export default TableRow;