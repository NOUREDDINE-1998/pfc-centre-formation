
import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class AddFormation extends Component {

    constructor(props){
        super(props);
        this.state={
            description:'',
            Prix:'',
            totalHeures:'',
            error:[],
        }

        
    }
    handleInput=(e)=>{
        this.setState({
            [e.target.name]:e.target.value
        });
    }
    saveFormation  = (e)=>{
        e.preventDefault();
         axios.post('http://127.0.0.1:8000/api/add-formation',this.state).then((res)=>{
            console.log(res);
            if(res.data.status===200){
                swal({
                    title: "Success!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });

                this.setState({
                description:'',
                prix:'',
                totalHeures:'',
                })
             //  this.props.history.push('/');
           
               
               
            }
            else{
                this.setState({
                    error:res.data.validateError,
                })
             }
         })
         
    }
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                                Formation Data
                            <Link to={'/formation'} className='btn btn-primary float-end btn-sm'> back   </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={this.saveFormation} method="post">
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <input type="text" className="form-control" id="description" name='description' value={this.state.description} onChange={this.handleInput} />
                            <span className='text-danger'>{this.state.error.description}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="prix" className="form-label">Prix</label>
                            <input type="text" className="form-control" id="prix" name='prix' value={this.state.prix} onChange={this.handleInput} /> 
                            <span className='text-danger'>{this.state.error.prix}</span>
                                </div>
                                <div className="mb-3">
                                <label htmlFor="totalHeures" className="form-label">Total Heures</label>
                                <input type="number" className="form-control" id="totalHeures" name='totalHeures' value={this.state.totalHeures} onChange={this.handleInput} /> 
                                <span className='text-danger'>{this.state.error.totalHeures}</span>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <button type="submit" className="btn btn-primary">save</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddFormation;