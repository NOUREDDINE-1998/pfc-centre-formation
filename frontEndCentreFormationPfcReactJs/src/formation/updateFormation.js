import React, { useState , useEffect}  from 'react';
import  { Link ,useParams  } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function UpdateFormation (props){
    const [description, setDescription] = useState('');
   const [prix, setPrix] = useState('');
    const [totalHeures, setTotalHeures] = useState('');
    const [error, setError] = useState([]);

//  const [formation, setFormation] = useState([]);
    //get id from url 
    const params = useParams();

   
     useEffect(() => {       
           
           axios.get('http://127.0.0.1:8000/api/edit-formation/'+params.id)
           .then((res)=>{
            if(res.data.status===200){
               
                setDescription(res.data.formation.description)    
                setPrix(res.data.formation.prix)    
                setTotalHeures(res.data.formation.totalHeures)  
                
           }
           else if(res.data.status===404){
            swal({
                title: "Warning!",
                text: res.data.message,
                icon: "Warning",
                button: "Ok!",
              });

           }
           
           })
         
          },[params]);
     
          const updateFormation=(e)=>{
                 e.preventDefault();
                 axios.put('http://127.0.0.1:8000/api/update-formation/'+params.id,{
                    description: description,
                    prix: prix,
                    totalHeures:totalHeures
                })
                 .then((res)=>{
                    if(res.data.status===200){
                        swal({
                            title: "Updated!",
                            text: res.data.message,
                            icon: "success",
                            button: "Ok!",
                          });
        

                        setDescription('');
                        setPrix('');
                        setTotalHeures('');
                        console.log(res.data.message)
                    }
                    else{
                        setError(res.data.validateError)  ;
                       }
                 })
                
            }

    return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                    <div className="card">
                        <div className="card-header">
                            <h4>
                                Formation Data
                            <Link to={'/formation'} className='btn btn-primary float-end btn-sm'> back  </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={updateFormation} >
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <input type="text" className="form-control" id="description" name='description' value={description} onChange={(e)=>{setDescription(e.target.value)}} />
                            <span className='text-danger'>{error.description}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="prix" className="form-label">Prix</label>
                            <input type="text" className="form-control" id="prix" name='prix' value={prix} onChange={(e)=>{setPrix(e.target.value)}}/> 
                            <span className='text-danger'>{error.prix}</span>
                                </div>
                                <div className="mb-3">
                                <label htmlFor="totalHeures" className="form-label">Total Heures</label>
                                <input type="number" className="form-control" id="totalHeures" name='totalHeures' value={totalHeures} onChange={(e)=>{setTotalHeures(e.target.value)}} /> 
                                <span className='text-danger'>{error.totalHeures}</span>
                                </div>
                        
                        <button type="submit" className="btn btn-primary">update</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    
}

export default UpdateFormation;