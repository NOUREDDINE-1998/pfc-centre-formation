import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class TableRow extends Component {  

    constructor(props){
        super(props);
        this.state={
            description:'',
            nom:"",
            prenom:''
        }

        
       
    }

 componentDidMount =async()=>{      
    
       await axios.get('http://127.0.0.1:8000/api/edit-session/'+this.props.data.session_id).then((res)=>{

         if(res.data.status===200){

            this.setState({
                
                   description:res.data.session.description

                     }) 
                    } 
                    })
                   
            // get formateur nom ,prenom of this.props.data.formateur_id
           await axios.get('http://127.0.0.1:8000/api/edit-Beneficiary/'+this.props.data.beneficiary_id).then((res)=>{

             if(res.data.status===200){
                
                 this.setState({
                     nom:res.data.beneficiary.nom,
                     prenom:res.data.beneficiary.prenom,
               })
                            
           }
         })
     }

     

    delete=(e,id)=>{
        e.preventDefault();
        let deleted=e.currentTarget;
        deleted.innerText="deleted";
        axios.delete('http://127.0.0.1:8000/api/delete-inscription/'+id)
           .then((res)=>{
            if(res.data.status===200){
                swal({
                    title: "Deleted!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });
                deleted.closest('tr').remove();
         
           }
           })
    }
    componentWillUnmount(){
      //  window.location.reload(true);
    }
  
    render() {
        return (
            
            <tr>
            <td>{this.props.data.id}</td>
            <td>{this.state.prenom + ' '+ this.state.nom}</td>
            <td>{this.state.description} </td>
            <td>{this.props.data.dateInscription}</td>
            
            <td>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                            
            <Link to={'/update-inscription/'+this.props.data.id} className='btn btn-warning float-end btn-sm' > Edit</Link>
            <button type="button" className="btn btn-danger" onClick={(e)=>{this.delete(e,this.props.data.id)}}>Delete</button>    

            </div>
            </td>
            
            </tr> 
        );
    }
}

export default TableRow;