
import React, { useState ,useEffect}  from 'react';
import  { Link ,useParams  } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function UpdateInscription (props)  {
    const params = useParams();
    const [inscription, setInscription] = useState({
        session_id:'',
        beneficiary_id:'',
        dateInscription:'',
       
    });
     
     const [error, setError] = useState([]);
     const [sessionsList, setSessionsList] = useState([]);
     const [beneficiaryList, setBeneficiaryList] = useState([]);
    
     const listsessions= async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/sessions');        
        if(res.data.status===200){
            setSessionsList(res.data.sessions)
       }
     }
     
     const listbeneficiaires=async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/beneficiaires');

        if(res.data.status===200){
            setBeneficiaryList(res.data.beneficiaires)
       }
       console.log(res.data.beneficiaires)
    }

        useEffect(() => {
            listsessions();
            listbeneficiaires();
        }, []);


     const handleInput=(e)=>{
        e.persist();
          setInscription({...inscription,[e.target.name]:e.target.value})
    }
    
    useEffect(() => {       
           
        axios.get('http://127.0.0.1:8000/api/edit-inscription/'+params.id).then((res)=>{

         if(res.data.status===200){
             //console.log(res.data.session)
             setInscription(res.data.inscription);

        }
        else if(res.data.status===404){
         swal({
             title: "Warning!",
             text: res.data.message,
             icon: "Warning",
             button: "Ok!",
           });

        }
        
        })
      
       },[params]);
   
   
    const UpdateInscription = (e)=>{
        e.preventDefault();
        const formData = new FormData() ;
        formData.append('session_id', inscription.session_id);
        formData.append('beneficiary_id', inscription.beneficiary_id);
        formData.append('dateInscription', inscription.dateInscription);
       
         axios.post('http://127.0.0.1:8000/api/update-inscription/'+params.id,formData).then(res=>{
            console.log(res.data.status) ;     
            if(res.data.status===200){
                
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });

                  setInscription({...inscription,
                    session_id:'',
                    beneficiary_id:'',
                    dateInscription:'',
                });

            }
            else{
                setError(res.data.validateError);
            }
           
         }) 
    }
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                            Inscription Data
                            <Link to={'/inscription'} className='btn btn-primary float-end btn-sm'> back </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={UpdateInscription} >
                        <div className="mb-3">
                            <label htmlFor="beneficiary_id" className="form-label">Beneficiary</label>
                            <select className="form-select"   name='beneficiary_id' onChange={handleInput} value={inscription.beneficiary_id}>
                                <option >choisir beneficiary</option>
                                {
                                    beneficiaryList.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id}  >{value.prenom +' '+ value.nom}</option>
                                        )
                                    })
                                }
                                </select>
                            <span className='text-danger'>{error.beneficiary_id}</span>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="session_id" className="form-label">session</label>
                            <select className="form-select"   name='session_id' onChange={handleInput} value={inscription.session_id}>
                                <option >choisir session</option>
                                {
                                    sessionsList.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id} >{value.description}</option>
                                        )
                                    })
                                }
                                </select>
                            <span className='text-danger'>{error.session_id}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateInscription" className="form-label">Date Inscription</label>
                            <input type="date" className="form-control" id="dateInscription" name='dateInscription' value={inscription.dateInscription} onChange={handleInput} />
                            <span className='text-danger'>{error.dateInscription}</span>
                        </div>
                        <button type="submit" className="btn btn-primary">update</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }


export default UpdateInscription;