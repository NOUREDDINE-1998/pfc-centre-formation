import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Inscription extends Component {
    constructor(props){
        super(props);
        this.state={
            Inscriptions:[],
            loading:true,
            currentKeyword:''
        }
    }
componentDidMount(){
    this.getInscriptionsList(this.state.currentKeyword);
}

getInscriptionsList=async (keyword)=>{
    const res= await axios.get('http://127.0.0.1:8000/api/inscriptions/'+keyword);

        if(res.data.status===200){
            this.setState({
                Inscriptions: res.data.inscriptions,
               loading:false
              });
       } 
}

search=(keyword)=>{
    
    this.getInscriptionsList(keyword);
   this.setState({
        currentKeyword:keyword
   });
}
    render() {
        var InscriptionsTableHTML='';
        if(this.state.loading===true){
            InscriptionsTableHTML=<tr><td colSpan="7"><h2 >Loading...</h2></td></tr>
        }
        else if(this.state.Inscriptions.length===0){
            InscriptionsTableHTML=<tr><td colSpan="7" ><h2 className='text text-danger'>non Inscription trouvée</h2></td></tr>
        }
        else{
            InscriptionsTableHTML= this.state.Inscriptions.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }

        return (
            <div className='container-fluid'>
                <div className='row offset-1'>
                <SearchBar dsearch={this.search}/>
                    <div className='col-md-10 '>
                    <div className="card">
                        <div className="card-header">
                            <h4>
                            Inscription Data
                            <Link to={'/add-inscription'} className='btn btn-primary float-end btn-sm'> Add Inscription</Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover'>
                            <thead >
                                <tr>
                                <th>#</th>
                                <th>Beneficiaire</th>   
                                <th> Session</th>                            
                                <th>Date Inscription</th>
                                <th>Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                    {InscriptionsTableHTML}    
                            </tbody>
                     </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Inscription;