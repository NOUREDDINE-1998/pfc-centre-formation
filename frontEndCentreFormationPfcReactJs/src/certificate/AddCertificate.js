
import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class AddCertificate extends Component {

    constructor(props){
        super(props);
        this.state={
            inscription_id:'',
            centre:'SupMTI',
            dateObtention:'',
            error:[],
            inscriptions:[]
        }

        
    }
    handleInput=(e)=>{
        this.setState({
            [e.target.name]:e.target.value
        });
    }

     inscription= async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/inscriptions');        
        if(res.data.status===200){
            this.setState({
                inscriptions:res.data.inscriptions,
            })
       }
     }

    saveCertificate  = (e)=>{
        e.preventDefault();
         axios.post('http://127.0.0.1:8000/api/add-certificate',this.state).then((res)=>{
            console.log(res);
            if(res.data.status===200){
                swal({
                    title: "Success!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });

                this.setState({
                inscription_id:'',
                centre:'',
                dateObtention:'',
                })
             //  this.props.history.push('/');
           
            }
            else{
                this.setState({
                    error:res.data.validateError,
                })
             }
         })
         
    }
    componentDidMount(){
        this.inscription();
    }
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                            Certificate Data
                            <Link to={'/certificate'} className='btn btn-primary float-end btn-sm'> back   </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={this.saveCertificate} method="post">
                        <div className="mb-3">
                            <label htmlFor="inscription_id" className="form-label">inscription_id</label>
                            <select className="form-select"   name='inscription_id' onChange={this.handleInput} value={this.state.inscription_id}>
                                <option >choisir inscription_id</option>
                                {
                                   this.state.inscriptions.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id} >{value.id}</option>
                                        )
                                    })
                                }
                                </select>
                            <span className='text-danger'>{this.state.error.inscription_id}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="centre" className="form-label">centre</label>
                            <input type="text" className="form-control" id="centre" name='prix' value={this.state.centre} onChange={this.handleInput} /> 
                            <span className='text-danger'>{this.state.error.centre}</span>
                                </div>
                                <div className="mb-3">
                                <label htmlFor="dateObtention" className="form-label">date Obtention </label>
                                <input type="date" className="form-control" id="dateObtention" name='dateObtention' value={this.state.dateObtention} onChange={this.handleInput} /> 
                                <span className='text-danger'>{this.state.error.dateObtention}</span>
                                </div>
                                
                        <button type="submit" className="btn btn-primary">save</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddCertificate;