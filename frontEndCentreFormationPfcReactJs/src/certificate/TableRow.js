import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class TableRow extends Component {  

    constructor(props){
        super(props);
        this.state=({
        inscription_id:this.props.data.inscription_id,
        centre:this.props.data.centre,
        dateObtention:this.props.data.dateObtention,
        imprime:this.props.data.imprime,
        })
       
    }
    delete=(e,id)=>{
        e.preventDefault();
        let deleted=e.currentTarget;
        deleted.innerText="deleted";
        axios.delete('http://127.0.0.1:8000/api/delete-certificate/'+id)
           .then((res)=>{
            if(res.data.status===200){
                swal({
                    title: "Deleted!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });
                deleted.closest('tr').remove();
         
           }
           })
    }
    update= async(e,id)=>{
        e.preventDefault();
        let flag;
        if(this.state.imprime===0){
            this.setState({
                imprime:1
            })
            flag=1
            }
             else if(this.state.imprime===1){
                this.setState({
                    imprime:0
                })
                flag=0
            }
   
        await axios.post('http://127.0.0.1:8000/api/update-certificate/'+id,{
            inscription_id: this.state.inscription_id,
            centre: this.state.centre,
            dateObtention:this.state.dateObtention,
            imprime:flag
        })
           .then((res)=>{
            if(res.data.status===200){
               //swal('succes','res.data.message','succes');
           }
           })
    }
    

    render() {
        return (
            
            <tr>
            <td>{this.props.data.id}</td>
            <td>{this.props.data.inscription_id}</td>
            <td>{this.props.data.centre}</td>
            <td>{ this.props.data.dateObtention}</td>
            <td> <button type="button" className="btn btn-danger" onClick={(e)=>{this.update(e,this.props.data.id)}}
             value={this.state.imprime}>{this.state.imprime ===0?'non imprimé':'imprimé'}</button> </td>
            <td>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                            
                <Link to={'/update-certificate/'+this.props.data.id} className='btn btn-warning float-end btn-sm' > Edit</Link>
                <button type="button" className="btn btn-danger" onClick={(e)=>{this.delete(e,this.props.data.id)}}>Delete</button>    
               
            </div>
            </td>
            
            </tr> 
        );
    }
}

export default TableRow;