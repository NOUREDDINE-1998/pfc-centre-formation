
import React, { useState ,useEffect}  from 'react';
import  { Link ,useParams  } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function UpdateCertificate (props)  {
const params=useParams();
    const [certificate, setCertificate] = useState({
        inscription_id:'',
        centre:'',
        dateObtention:'',
        imprime:'',
    });
     
     const [error, setError] = useState([]);
     const [inscriptionList, setinscriptionList] = useState([]);
     

     const getCertificateDetails=async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/edit-certificate/'+params.id);    
        if(res.data.status===200){
            setCertificate(res.data.certificate)
       }
     }

     const inscription= async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/inscriptions');        
        if(res.data.status===200){
            setinscriptionList(res.data.inscriptions)
       }
     }

     
        useEffect(() => {
            getCertificateDetails();
            inscription();
        }, []);



     const handleInput=(e)=>{
        e.persist();
          setCertificate({...certificate,[e.target.name]:e.target.value})
    }
    
   
   
   
    const updateCertificate = (e)=>{
        e.preventDefault();    
          axios.post('http://127.0.0.1:8000/api/update-certificate/'+params.id,certificate).then(res=>{
            if(res.data.status===200){
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });
                    

                  setCertificate({...certificate,
                    inscription_id:'',
                    centre:'',
                    dateObtention:'',
                   
                });

            }
            else{
                setError(res.data.validateError);
            }
           
         })
         
    }
   
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                            certificate Data
                            <Link to={'/certificate'} className='btn btn-primary float-end btn-sm'> back </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={updateCertificate} >

                        <div className="mb-3">
                            <label htmlFor="inscription_id" className="form-label">inscription_id</label>
                            <select className="form-select"   name='inscription_id' onChange={handleInput} value={certificate.inscription_id}>
                                <option >choisir inscription_id</option>
                                {
                                    inscriptionList.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id} >{value.id}</option>
                                        )
                                    })
                                }
                                </select>
                            <span className='text-danger'>{error.inscription_id}</span>
                        </div>
                                   
                        <div className="mb-3">
                            <label htmlFor="centre" className="form-label">centre</label>
                            <input type="text" className="form-control" id="centre" name='centre' value={certificate.centre} onChange={handleInput} />
                            <span className='text-danger'>{error.centre}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateObtention" className="form-label">date Obtention</label>
                            <input type="date" className="form-control" id="dateObtention" name='dateObtention' value={certificate.dateObtention} onChange={handleInput} />
                            <span className='text-danger'>{error.dateObtention}</span>
                        </div>
                        <button type="submit" className="btn btn-primary">update</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }


export default UpdateCertificate;