import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Certificate extends Component {
    constructor(props){
        super(props);
        this.state={
            certificates:[],
            loading:true,
            currentKeyword:''
        }
    }
componentDidMount(){
    this.getCertificateList(this.state.currentKeyword);
}

getCertificateList=async (keyword)=>{
    const res= await axios.get('http://127.0.0.1:8000/api/certificates/'+keyword);

        if(res.data.status===200){
            this.setState({
                certificates: res.data.certificates,
               loading:false
              });
       } 
}

search=(keyword)=>{
    
    this.getCertificateList(keyword);
   this.setState({
        currentKeyword:keyword
   });
}
    render() {
        var CertificatesTableHtml='';
        if(this.state.loading===true){
            CertificatesTableHtml=<tr><td colSpan="7"><h2>Loading...</h2></td></tr>
        }
        else{
            CertificatesTableHtml= this.state.certificates.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }


        return (
            <div className='container '>
                <div className='row offset-1'>
                <SearchBar dsearch={this.search}/>

                    <div className='col-md-10 '>
                    <div className="card">
                        <div className="card-header">
                           
                            <h4>
                                Certificate Data
                            <Link to={'/add-certificate'} className='btn btn-primary float-end btn-sm'> Add Certificate</Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover'>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>inscription_id</th>
                    <th>centre </th>
                    <th>date Obtention </th>
                    <th>statut  </th>
                    <th>Actions</th>
                    
                    </tr>
                </thead>
                <tbody>
                   
                         {CertificatesTableHtml}    
                </tbody>
            </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Certificate;