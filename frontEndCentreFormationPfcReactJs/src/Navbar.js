import React, { Component } from 'react';
import { Link ,useNavigate} from  'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
 const Navbar =()=> {
    const navigate=useNavigate();

const logoutUser=(e)=>{
    e.preventDefault();
    
    swal({
        title: "log out! Are you sure? ",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            axios.post('api/logout').then((res)=>{
                if(res.data.status===200){
                    localStorage.removeItem('auth_token');
                    localStorage.removeItem('auth_username');
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });
                   navigate("/");
                }
               
            })
         }
      });
}


var authButton='';
var navabrMenu=''
var paymentRole=''
if(!localStorage.getItem('auth_token')){
    
    authButton=(
       <ul className='navbar-nav ms-auto '>
         <li className="nav-item ">
        <Link className='nav-link' to="/login">Login</Link>
        </li>
        
       </ul>
    );


}
else{
    
    authButton=(
        <ul className='navbar-nav ms-auto '>
             <li className="nav-item me-5">
                   <h5 className='text-white '><strong>Username: {localStorage.getItem('auth_username')}</strong></h5>
                    </li>
        <li className="nav-item ">
        <button type='button' onClick={logoutUser} className='nav-link btn btn-danger btn-sm text-white'>Logout</button>
        </li>
        </ul>
    );
    
    navabrMenu=(
        <ul className="navbar-nav ">
            {localStorage.getItem('auth_role')==='2' ?  "" :
        <li className="nav-item">
        <Link className='nav-link' to="/">Home</Link>
        </li>}
        {localStorage.getItem('auth_role')==='2' ?  '':
        <li className="nav-item">
        <Link className='nav-link' to="/formation">Formation</Link>
        </li>}
        {localStorage.getItem('auth_role')==='2' ?  '':
        <li className="nav-item">
        <Link className='nav-link' to="/formateur">Formateur</Link>
        </li>}
        {localStorage.getItem('auth_role')==='2' ?  '':
        <li className="nav-item">
        <Link className='nav-link' to="/beneficiaire">Beneficiaire</Link>
        </li>}
        {localStorage.getItem('auth_role')==='2' ?  '':
        <li className="nav-item">
        <Link className='nav-link' to="/session">Session</Link> 
        </li>}
        {localStorage.getItem('auth_role')==='2' ?  ' ':
        <li className="nav-item">
        <Link className='nav-link' to="/inscription">Inscription</Link>
        </li>}
        {localStorage.getItem('auth_role')==='2' ?  '':
        <li className="nav-item ">
         <Link className='nav-link' to="/payment">Payment</Link>
        </li>}
    
                {localStorage.getItem('auth_role')==='1' ? <li className="nav-item">
                <Link className='nav-link' to="/certificate">Certificate</Link>
                </li>       : " "  
                }

                {localStorage.getItem('auth_role')==='2' ?  
                <li className="nav-item ">
                <Link className='nav-link' to="/register">Register</Link>
                </li>
                : ""
                }
    
    </ul>
    );

}

 

        return (
            
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark  mb-3  ">
            <div className="container">
                <Link className="navbar-brand " to="/">
                <img src=".\logoCenter.webp" alt="test" width="100" height="100"/>
                     </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse " id="navbarNavDropdown">

                {navabrMenu }
           
                {authButton}

            
                </div>
            </div>
        </nav>
        );
    }


export default Navbar;