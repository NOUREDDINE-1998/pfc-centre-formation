import React, { Component } from 'react';


class SearchBar extends Component {
constructor(props){
    super(props);
    this.state={
        keyword:''
    }
}
setKeyword=(event)=>{
    this.setState({
        Keyword:event.target.value
    })
    
}
search=(event)=>{
    event.preventDefault();
    this.props.dsearch(this.state.keyword);
}

    render() {
        return (
            <form onSubmit={this.search}>
            <div className='row mb-3'>
                <div className='col-md-10'>
                    <input type="search"  placeholder="Search"
                    value={this.state.keyword}  
                    onChange={e=>this.setState({ keyword:e.target.value})} className='form-control'/>
                </div>
                <div className='col-auto'>
                    <button className='btn btn-success' type='submit'>Search</button>
                </div>
            </div>
        </form>
            
        );
    }
}

export default SearchBar;