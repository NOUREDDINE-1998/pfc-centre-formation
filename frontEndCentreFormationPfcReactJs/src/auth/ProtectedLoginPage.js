import React from 'react';
import {Outlet, Navigate  } from "react-router";

const ProtectedLoginPage = () => {
    return localStorage.getItem('auth_token')? <Navigate to='/' />: <Outlet/> ;
};

export default ProtectedLoginPage;