import React, { useState } from 'react';
import axios from 'axios';
import {useNavigate} from 'react-router-dom';
import swal from 'sweetalert';

const Login = () => {
    const navigate = useNavigate();
    const [error, setError] = useState([]);
    const [login, setLogin] = useState({
        email:'',
        password:''
    });

const handleInput=(e)=>{
    e.persist();
    setLogin({...login,[e.target.name]:e.target.value})
}

const loginUser=(e)=>{
    e.preventDefault();
    const data={
        email:login.email,
        password:login.password,
        
    }
    
axios.get('sanctum/csrf-cookie').then(() => {
    axios.post('api/login',data).then((res)=>{
        if(res.data.status===200){
            localStorage.setItem('auth_token',res.data.token);
            localStorage.setItem('auth_username',res.data.username);
            localStorage.setItem('auth_role',res.data.role);
            swal({
                title: "Success!",
                text: res.data.message,
                icon: "success",
                button: "Ok!",
            });
            navigate("/");
        }
        else if(res.data.status===401){
            swal('Warning',res.data.message,"warning");
        }
        else{
            setError(res.data.validateError);
        }
    })
});
}
    return (
        <div className='container py-5'>
        <div className='row'>
            <div className='col-md-6 offset-3'>
            <div className="card">
                <div className="card-header">
                
                    <h4>
                   Authentification
                    </h4>  
                </div>
                <div className="card-body">
            <form onSubmit={loginUser} >
               
                <div className="mb-3">
                    <label htmlFor="email" className="form-label">Email:</label>
                    <input type="email" className="form-control" id="email" name='email' value={login.email} onChange={handleInput} />
                    <span className='text-danger'>{error.email}</span>
                </div>
                <div className="mb-3">
                    <label htmlFor="password" className="form-label">Password: </label>
                    <input type="password" className="form-control" id="password" name='password' value={login.password} onChange={handleInput} />
                    <span className='text-danger'>{error.password}</span>
                </div>
                <button type="submit" className="btn btn-primary">login</button>
            </form>
                    
                </div>
            </div>
            </div>
        </div>
    </div>
    );
};

export default Login;