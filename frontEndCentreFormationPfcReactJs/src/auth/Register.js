import React, { useState }  from 'react';
import axios from 'axios';
import swal from 'sweetalert';
import {useNavigate} from 'react-router-dom';
const Register = (props) => {
const navigate = useNavigate();
    const [error, setError] = useState([]);
    const [register, setregister] = useState({
        name:'',
        email:'',
        password:'',
        role_as:'',
    });

const handleInput=(e)=>{
e.persist();
setregister({...register,[e.target.name]:e.target.value})
}
const registerUser=(e)=>{
    e.preventDefault();
    const data={
        name:register.name,
        email:register.email,
        password:register.password,
        role_as:register.role_as,
    }
    axios.get('sanctum/csrf-cookie').then(response => {
        axios.post('api/register',data).then((res)=>{
            if(res.data.status===200){
               // localStorage.setItem('auth_token',res.data.token);
                //localStorage.setItem('auth_username',res.data.username);
                swal({
                    title: "Success!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                });
                navigate("/");
            }
            else{
                setError(res.data.validateError);
            }
        })
    });
}
    return (
        <div className='container py-5'>
        <div className='row'>
            <div className='col-md-6 offset-3'>
            <div className="card">
                <div className="card-header">               
                    <h4>
                   Register
                    </h4>  
                </div>
                <div className="card-body">
            <form  onSubmit={registerUser}>
            <div className="mb-3">
                    <label htmlFor="name" className="form-label">Full Name:</label>
                    <input type="text" className="form-control" id="name" name='name' value={register.name} onChange={handleInput} />
                    <span className='text-danger'>{error.name}</span>
                </div>
                <div className="mb-3">
                    <label htmlFor="email" className="form-label">Email:</label>
                    <input type="email" className="form-control" id="email" name='email' value={register.email} onChange={handleInput} />
                    <span className='text-danger'>{error.email}</span>
                </div>              
                <div className="mb-3">
                    <label htmlFor="password" className="form-label">Password: </label>
                    <input type="password" className="form-control" id="password" name='password' value={register.password} onChange={handleInput} />
                    <span className='text-danger'>{error.password}</span>
                </div>
               
                <div className="mb-3">
                            <label htmlFor="role_as" className="form-label">Role</label>
                            <select className="form-select"   name='role_as' onChange={handleInput} value={register.role_as}>
                                <option >choisir role</option>  
                                <option  value={0} key={0}  >Secretaire</option>
                                <option  value={1} key={1}  >Directeur</option>
                                <option  value={2} key={2}  >Administrateur</option>
                                </select>
                            <span className='text-danger'>{error.role_as}</span>
                        </div>
                <button type="submit" className="btn btn-primary">register</button>
            </form>
                    
                </div>
            </div>
            </div>
        </div>
    </div>
    );
};

export default Register;