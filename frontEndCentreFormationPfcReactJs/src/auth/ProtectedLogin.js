import React from 'react';
import {Outlet, Navigate  } from "react-router";

const ProtectedLogin = () => {


    return localStorage.getItem('auth_token')? <Outlet/> : <Navigate to='/login'/>;
};

export default ProtectedLogin;