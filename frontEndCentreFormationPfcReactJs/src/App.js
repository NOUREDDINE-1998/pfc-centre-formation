import {BrowserRouter, Routes, Route, Link , Navigate ,useNavigate } from "react-router-dom";
import * as React from "react";
import {  useEffect } from 'react';

import axios from 'axios';
import './App.css';
import Formation from './formation/formations'
import AddFormation from './formation/addFormation'
import UpdateFormation from './formation/updateFormation'
import Navbar from './Navbar'
import Home from './Home'
import Formateur from './formateur/formateurs'
import AddFormateur from "./formateur/addFormateur";
import UpdateFormateur from "./formateur/updateFormateur";
import Beneficiaire from "./beneficiaire/beneficiaires";
import AddBeneficiaire from "./beneficiaire/addBeneficiaire";
import UpdateBeneficiaire from "./beneficiaire/updateBeneficiaire";
import Session from "./session/sessions";
import AddSession from "./session/addSession";
import UpdateSession from "./session/updateSession";
import Inscription from "./inscription/inscriptions";
import AddInscription from "./inscription/addInscription";
import UpdateInscription from "./inscription/updateInscription";
import Payment from "./payment/payments";
import AddPayment from "./payment/addPayment";
import UpdatePayment from "./payment/updatePayment";
import Example from "./payment/print";
import Certificate from "./certificate/certificate";
import AddCertificate from "./certificate/AddCertificate";
import UpdateCertificate from "./certificate/UpdateCertificate";
import Login from "./auth/Login";
import Register from "./auth/Register";
import swal from "sweetalert";
import ProtectedLogin from "./auth/ProtectedLogin";
import ProtectedLoginPage from "./auth/ProtectedLoginPage";
//to solve prob of csrf
axios.defaults.withCredentials = true;
axios.defaults.baseURL="http://127.0.0.1:8000/";
axios.defaults.headers.post['Content-Type']='application/json';
axios.defaults.headers.post['Accept']='application/json';

 axios.interceptors.request.use(function(config){
  const token=localStorage.getItem('auth_token');
  if (token) {
    config.headers['Authorization'] = 'Bearer ' + token
  }
   return config;
})
function App() {
  //const navigate = useNavigate();
  axios.interceptors.response.use(undefined,function axiosRestryInterceptors(err){
    if(err.response.status===401){
      swal("unauthorized",err.response.data.message,'warning');
      //navigate("/");
      return;
    
    }
    return Promise.reject(err);
  })
 
  return (
    <BrowserRouter>
    <Navbar/>

    <Routes>
      <Route element={<ProtectedLogin/>}>
          {/* Formation */}
            
            <Route path="/formation" element={<Formation />} />
            <Route path="/add-formation" element={<AddFormation />} />
            <Route path="/update-formation/:id" element={<UpdateFormation />} />
            {/*formateur  */}
            <Route path="/formateur" element={<Formateur />} />
            <Route path="/add-formateur" element={<AddFormateur />} />      
            <Route path="/update-formateur/:id" element={<UpdateFormateur />} />  

            {/*beneficiaires  */}
            <Route path="/beneficiaire" element={<Beneficiaire/>} />
            <Route path="/add-beneficiaire" element={<AddBeneficiaire />} />   
            <Route path="/update-beneficiaire/:id" element={<UpdateBeneficiaire />} />   
            {/*Session  */}
            <Route path="/session" element={<Session/>} />
            <Route path="/add-session" element={<AddSession />} /> 
            <Route path="/update-session/:id" element={<UpdateSession/>} /> 
            
              {/*inscription  */} 
              <Route path="/inscription" element={<Inscription/>} />
              <Route path="/add-inscription" element={<AddInscription />} />
              <Route path="/update-inscription/:id" element={<UpdateInscription />} /> 

              {/*payment  */} 
              <Route path="/payment" element={<Payment/>} />
              <Route path="/add-payment" element={<AddPayment />} />
              <Route path="/update-payment/:id" element={<UpdatePayment/>} /> 
              <Route path="/print/:id" element={<Example/>} />  

                {/*certificate  */} 
                <Route path="/certificate" element={<Certificate/>} />
              <Route path="/add-certificate" element={<AddCertificate />} />
              <Route path="/update-certificate/:id" element={<UpdateCertificate/>} />           
          </Route>
            
             
             {/*login  */} 
             <Route path="/" element={<Home />} />

             <Route element={<ProtectedLoginPage/>}>
                <Route  path="/login" element={<Login/>} />
              </Route>   
            <Route path="/register" element={<Register/>} />
           

      </Routes>
      </BrowserRouter>
  );
}

export default App;
