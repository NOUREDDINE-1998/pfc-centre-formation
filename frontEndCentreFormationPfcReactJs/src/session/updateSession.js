
import React, { useState ,useEffect}  from 'react';
import  { Link,useParams   } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

function UpdateSession (props)  {

    const params = useParams();
    const [session, setSession] = useState({
        formateur_id:'',
        formation_id:'',
        description:'',
        dateDebut:'',
        dateFin:'',
        horaire:'',
       
    });
     
     const [error, setError] = useState([]);
     const [formateursList, setFormateursList] = useState([]);
     const [formationsList, setFormationsList] = useState([]);
    
     const listFormateurs= async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/formateurs');        
        if(res.data.status===200){
            setFormateursList(res.data.formateurs)
       }
     }
     
     const listFormations=async()=>{
        const res= await axios.get('http://127.0.0.1:8000/api/formations');

        if(res.data.status===200){
            setFormationsList(res.data.formations)
       }
       console.log(res.data.formations)
    }
        useEffect(() => {
            listFormateurs();
            listFormations();
        }, []);

     const handleInput=(e)=>{
        e.persist();
          setSession({...session,[e.target.name]:e.target.value})
    }
    
    useEffect(() => {       
           
        axios.get('http://127.0.0.1:8000/api/edit-session/'+params.id).then((res)=>{

         if(res.data.status===200){
             //console.log(res.data.session)
             setSession(res.data.session);

        }
        else if(res.data.status===404){
         swal({
             title: "Warning!",
             text: res.data.message,
             icon: "Warning",
             button: "Ok!",
           });

        }
        
        })
      
       },[params]);
   
   
   
    const saveSession = (e)=>{
        e.preventDefault();
        const formData = new FormData() ;
        formData.append('formateur_id', session.formateur_id);
        formData.append('formation_id', session.formation_id);
        formData.append('description', session.description);
        formData.append('dateDebut', session.dateDebut);
        formData.append('dateFin', session.dateFin);
        formData.append('horaire', session.horaire);

         axios.post('http://127.0.0.1:8000/api/update-session/'+params.id,formData).then(res=>{
            console.log(res.data.status) ;     
            if(res.data.status===200){
                
                    swal({
                        title: "Success!",
                        text: res.data.message,
                        icon: "success",
                        button: "Ok!",
                    });
                    

                  setSession({...session,
                    formateur_id:'',
                    formation_id:'',
                    description:'',
                    dateDebut:'',
                    dateFin:'',
                    horaire:'',
                });

            }
            else{
                setError(res.data.validateError);
            }
           
         })
         
    }
   
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-md-8'>
                    <div className="card">
                        <div className="card-header">
                        
                            <h4>
                            Session Data
                            <Link to={'/session'} className='btn btn-primary float-end btn-sm'> back </Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                    <form onSubmit={saveSession} >
                        <div className="mb-3">
                            <label htmlFor="formateur_id" className="form-label">formateur</label>
                            <select className="form-select"   name='formateur_id' onChange={handleInput} value={session.formateur_id}>
                                <option >choisir formateur</option>
                                {
                                    formateursList.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id}  >{value.prenom +' '+ value.nom}</option>
                                        )
                                    })
                               
                                }
                        
                                </select>
                            <span className='text-danger'>{error.formateur_id}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="formation_id" className="form-label">formation</label>
                            <select className="form-select"   name='formation_id' onChange={handleInput} value={session.formation_id}>
                                <option >choisir formation</option>
                                {
                                    formationsList.map((value)=>{
                                        return(
                                            <option  value={value.id} key={value.id}  >{value.description}</option>
                                        )
                                    })
                               
                                }
                        
                                </select>
                            <span className='text-danger'>{error.formation_id}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <input type="text" className="form-control" id="description" name='description' value={session.description} onChange={handleInput} />
                            <span className='text-danger'>{error.description}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateDebut" className="form-label">date Debut</label>
                            <input type="date" className="form-control" id="dateDebut" name='dateDebut' value={session.dateDebut} onChange={handleInput}/>
                            <span className='text-danger'>{error.dateDebut}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="dateFin" className="form-label">date Fin</label>
                            <input type="date" className="form-control" id="dateFin" name='dateFin' value={session.dateFin} onChange={handleInput} />
                            <span className='text-danger'>{error.dateFin}</span>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="horaire" className="form-label">Horaire</label>
                            <input type="text" className="form-control" id="horaire" name='horaire' value={session.horaire} onChange={handleInput}/>
                            <span className='text-danger'>{error.horaire}</span>
                        </div>                           
                       
                      
                        <button type="submit" className="btn btn-primary">update</button>
                    </form>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }


export default UpdateSession;