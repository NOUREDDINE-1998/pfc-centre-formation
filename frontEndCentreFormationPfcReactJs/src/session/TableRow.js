import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';

class TableRow extends Component {  

    constructor(props){
        super(props);
        this.state={
            description:'',
            nom:"",
            prenom:''
        }
       
    }

    componentDidMount(){
        // get description formation of this.props.data.formation_id 
            axios.get('http://127.0.0.1:8000/api/edit-formation/'+this.props.data.formation_id )
           .then((res)=>{
            if(res.data.status===200){
                this.setState({
                    description:res.data.formation.description
                })
                // console.log(this.state.description)         
           }})
           // get formateur nom ,prenom of this.props.data.formateur_id
           axios.patch('http://127.0.0.1:8000/api/edit-formateur/'+this.props.data.formateur_id).then((res)=>{

            if(res.data.status===200){
                
                this.setState({
                    nom:res.data.formateur[0].nom,
                    prenom:res.data.formateur[0].prenom,
                })
                

           }
        })
    }


    delete=(e,id)=>{
        e.preventDefault();
        let deleted=e.currentTarget;
        deleted.innerText="deleted";
        axios.delete('http://127.0.0.1:8000/api/delete-session/'+id)
           .then((res)=>{
            if(res.data.status===200){
                swal({
                    title: "Deleted!",
                    text: res.data.message,
                    icon: "success",
                    button: "Ok!",
                  });
                deleted.closest('tr').remove();
         
           }
           })
    }
  
    render() {
        return (
            
            <tr>
            <td>{this.props.data.id}</td>
            <td>{this.state.prenom + ' '+ this.state.nom}</td>
            <td>{this.state.description} </td>
            <td>{this.props.data.description}</td>
            <td>{this.props.data.dateDebut}</td>
            <td>{this.props.data.dateFin}</td>
            <td>{this.props.data.horaire}</td>
            <td>
            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                            
            <Link to={'/update-session/'+this.props.data.id} className='btn btn-warning float-end btn-sm' > Edit</Link>
            <button type="button" className="btn btn-danger" onClick={(e)=>{this.delete(e,this.props.data.id)}}>Delete</button>    

            </div>
            </td>
            
            </tr> 
        );
    }
}

export default TableRow;