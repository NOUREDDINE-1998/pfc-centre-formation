import React, { Component } from 'react';
import  { Link } from 'react-router-dom';
import axios from 'axios';
import TableRow from './TableRow';
import SearchBar from '../SearchBar';
class Session extends Component {
    constructor(props){
        super(props);
        this.state={
            Sessions:[],
            loading:true,
            currentKeyword:''
        }
    }
componentDidMount(){
    this.getSessionsList(this.state.currentKeyword);
}

getSessionsList=async (keyword)=>{
    const res= await axios.get('http://127.0.0.1:8000/api/sessions/'+keyword);

        if(res.data.status===200){
            this.setState({
                Sessions: res.data.sessions,
               loading:false
              });
       } 
}

search=(keyword)=>{
    
    this.getSessionsList(keyword);
   this.setState({
        currentKeyword:keyword
   });
}
    render() {
        var SessionsTableHTML='';
        if(this.state.loading===true){
            SessionsTableHTML=<tr><td colSpan="7"><h2 >Loading...</h2></td></tr>
        }
        else if(this.state.Sessions.length===0){
            SessionsTableHTML=<tr><td colSpan="7" ><h2 className='text text-danger'>non Session trouvé</h2></td></tr>
        }
        else{
            SessionsTableHTML= this.state.Sessions.map(function (x, i){
             return  <TableRow key={i} data={x}/>            
                })
        }

        return (
            <div className='container-fluid'>
                <div className='row'>
                <SearchBar dsearch={this.search}/>

                    <div className='col-md-12 '>
                    <div className="card">
                        <div className="card-header">
                            <h4>
                            Session Data
                            <Link to={'/add-session'} className='btn btn-primary float-end btn-sm'> Add Session</Link>
                            </h4>  
                        </div>
                        <div className="card-body">
                        <table className=' table table-hover'>
                            <thead >
                                <tr>
                                <th>#</th>
                                <th> formateur</th>
                                <th>formation</th>
                                <th>description </th>
                                <th>dateDebut</th>
                                <th>dateFin</th>
                                <th>horaire </th>
                                <th>Actions </th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                    {SessionsTableHTML}    
                            </tbody>
                     </table>
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Session;